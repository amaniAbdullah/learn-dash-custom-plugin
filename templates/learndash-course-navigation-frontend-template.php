<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="learndash-course-navigation-widget">

        <div class="breadcrumbs-container  <# if(settings.show_breadcrumbs!='yes' ){ #> hide-content<# } #>">
            <ul>
                <# if(settings.custom_label_breadcrumbs=="yes" ){ #>
                    <li class="widget-course-navigation-breadcrumbs-style ">
                        <# if(settings.link_breadcrumbs.url){ #>
                            <a href="{{ settings.link_breadcrumbs.url }}" {{ target }}{{ nofollow }}> {{settings.custom_label_breadcrumbs_text}} ></a>
                            <# } else{ #>
                                {{settings.custom_label_breadcrumbs_text}} >
                                <# } #>
                    </li>
                    <# } #>
                        <li class="widget-course-navigation-breadcrumbs-style"><a href="#">Course ></a></li>
                        <li class="widget-course-navigation-breadcrumbs-style"><a href="#">Module ></a></li>
                        <li class="widget-course-navigation-breadcrumbs-style active"><a href="#">topic </a></li>
            </ul>
        </div>

            <div class="accordion-option accordion">
                <# var i=0; var rand=Math.floor(Math.random() * 10000); var x=0; _.each(course_modules , function(course_module){ if( course_module.course_id==course_id){ #>
                    <div class="ac-container">
                        <div class="ac-title <# if(x == 0){ #> open-collapse <# } #>" id="{{rand}}_{{x}}_title">
                            <span class="widget-course-navigation-icon-collapse-style collapse">
                                {{{iconCollapse.value}}}
                            </span>
                            <span class="widget-course-navigation-lessons-style">
                                <a href="{{course_module.module_URL}}"> {{course_module.module_title}}</a>
                            </span>
                                <span class="progress-content-container ">
                                    <# if(course_module.steps == 100){ #>
                                <span class="widget-course-navigation-icon-course-completed-style course-completed">
                                {{{iconCourseCompleted.value}}}
                                </span>
                                <# } else { #>
                                    <span class="circle-progress  learndash-course-navigation-widget-circle-progress-style">
                                        <span class="ProgressBar ProgressBar--animateNone" data-animation-duration="2000" data-progress="<# if (course_module.steps != null){ #>{{course_module.steps}}<# } else { #>0<# } #>">
                                            <svg class="ProgressBar-contentCircle" viewBox="-4 -4 220 220">
                                                <circle transform="rotate(-90, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="95" />
                                                <circle transform="rotate(-90, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="95" />
                                                <span class="ProgressBar-percentage ProgressBar-percentage--count hide-visibility-content"></span>
                                            </svg>
                                        </span>
                                    </span>
                                    <# } #>
                                </span>

                        </div>
                        <div class="ac-content <# if(x != 0){ #> hide-content <# } #>" id="{{rand}}_{{x}}_content">
                            <ul>
                                <# _.each(course_modules_lessons ,function(course_modules_lesson){ if( course_modules_lesson.course_id==course_id && course_modules_lesson.module_id==course_module.module_id){ if(course_modules_lesson.topic_user_status=='complete' ){ #>
                                    <li>
                                        <a class=" widget-course-navigation-topics-style border widget-course-navigation-topics-background" href='{{course_modules_lesson.topic_premalink}}'>
                                            <span class="widget-course-navigation-topics-style-text">{{course_modules_lesson.topic_title}}</span>
                                            <span class="widget-course-navigation-icon-style completed">
                                                {{{iconCompleted.value}}}
                                            </span>
                                        </a>
                                    </li>
                                    <# } else{ #>
                                        <li>
                                            <a class=" widget-course-navigation-topics-style border widget-course-navigation-topics-background" href='{{course_modules_lesson.topic_premalink}}'>
                                                <span class="widget-course-navigation-topics-style-text">{{course_modules_lesson.topic_title}}</span>
                                                <span class="widget-course-navigation-icon-style incomplete">
                                                    {{{iconIncomplete.value}}}
                                                </span>
                                            </a>
                                        </li>
                                        <# } } }) #>
                            </ul>
                        </div>
                    </div>
                    <# x++; i++; } }) #>
            </div>

</div>