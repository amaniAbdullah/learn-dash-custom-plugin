<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div class="learndash-duration-overview-widget">
    <div class="title-value-container">
        <div class="flex-container">
            <div class="title">
                <h3 class="learndash-duration-overview-title-text">
                <#
			if(settings.title){ #>  {{settings.title}}
			 <# } #>
                </h3>
                <div class="icon-tooltip learndash-duration-overview-widget-icon-style">
                   {{{selectedIcon.value}}}
                    <div class="tooltip"><#
			if(settings.tooltip){ #>  {{{settings.tooltip}}}
			 <# } #></div>
                </div>
            </div>
            <div class="value learndash-duration-overview-value-text">5 Tage</div>
        </div>
    </div>
    <div class="progress-bar-container">
        <div class="progress-bar-background learndash-duration-overview-widget-bar-style {{rounded_corner}} ">
         <div class="progress-bar-value learndash-duration-overview-widget-value-style {{rounded_corner}}" style="width:50%; animation-duration= {{settings.progress_animation_duration.size}} s"></div>
        </div>
    </div>
    <div class="finish-text-container">
        <h4 class="learndash-duration-overview-finish-text">
			<#
			if(settings.finish_text){ #>  {{settings.finish_text}}
			 <# } #>
		</h4>
    </div>