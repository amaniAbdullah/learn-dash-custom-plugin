<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
 
?>
<!-- start of widget container -->
    <div class="learndash-circle-progress-widget">
			<div class="main-progress-circle-widget-container">
			<?php  
			$this->get_courses(); 
			/**
			 * start of text top container
			 */
			if($settings['label_position'] === "top"){ ?>
			<div class="text-container">
				<div class="main-title learndash-circle-progress-widget-circle-title-style ">
					<?php 
			echo '<h2>' . $settings['title'] . '</h2>'; 
			?>
				</div>
				<div class="sub-title learndash-circle-progress-widget-circle-subtitle-style">
					<?php 
			if($settings['link_subtitle'] !== Null){
				echo '<a href="' . $settings['link_subtitle']['url'] . '"' . $target . $nofollow . '> <h3>' . $settings['subtitle'] . '</h3></a>';
			}
			else{
				echo '<h3>' . $settings['subtitle'] . '</h3>'; 
			}
			?>
				</div> 	
			</div>
			<?php } 
			/**
			 * end of text top container
			 */
			?>
			<!-- start of circle container -->
			 
			<div class="circle-conteiner">
				<div class="circle-progress">
					<div class="ProgressBar ProgressBar--animateNone"  
					data-progress="	<?php  
								if ( 'yes' === $settings['set_value'] ) {
									echo $settings['value_number_prefix'];  }
								else {  echo $course_progress['user_course_progress'];} 
							?> ">
							<style>
						.learndash-circle-progress-widget .circle-progress .ProgressBar-circle{
								animation: dash <?php echo $settings['progress_animation_duration']['size'];?>s ease-in-out; 
								-webkit-animation: dash <?php echo $settings['progress_animation_duration']['size'];?>s ease-in-out;
								 -moz-animation: dash <?php echo $settings['progress_animation_duration']['size'];?>s ease-in-out;
							}
						</style>
						<svg class="ProgressBar-contentCircle learndash-circle-progress-widget-circle-style"  viewBox="-21 -21 246 246"> 
						<defs class="learndash-circle-progress-back-stroke-style">
						<linearGradient id="Gradient1">
							<stop class="stop-1" offset="0%"/> 
							<stop class="stop-2" offset="100%"/>
						</linearGradient>
						<linearGradient id="Gradient2">
							<stop class="stop-1" offset="0%"/> 
							<stop class="stop-2" offset="100%"/>
						</linearGradient>	
						</defs>
						<circle style="stroke:url(#Gradient1);" transform="rotate(-90, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="<?php echo  $settings['stroke_back_position']['size']; ?>" />    
							
						<circle style="stroke:url(#Gradient2); stroke-linecap: <?php echo $settings['value_stroke_linecap']; ?>;" transform="rotate(-90, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="<?php echo  $settings['value_stroke_position']['size']; ?>" />
							<span class="ProgressBar-percentage learndash-circle-progress-widget-circle-text-style">
							<?php 
							if($settings['show_value'] ==='yes'){
								if ( 'yes' === $settings['set_value'] ) { ?>
									<span class="prefix"><?php echo $settings['value_number_prefix']; ?></span>
								<span class="suffix"><?php echo  $settings['value_number_suffix']; ?> </span>
								<?php }
								else {
								?>
							 <span class="prefix"><?php echo $course_progress['user_course_progress']; ?></span>
								<span class="suffix"><?php echo '%'; ?></span>
								<?php
								}
							}
							?> 
							</span>
						</svg>
					</div> 
				</div>
			</div>
			<!-- end of circle container -->
			<?php
				/**
				 * start of text bottom container
				 */
				if($settings['label_position'] === "default"){ ?>
			<div class="text-container">
				<div class="main-title learndash-circle-progress-widget-circle-title-style ">
					<?php 
			echo '<h2>' . $settings['title'] . '</h2>'; 
			?>
				</div>
				<div class="sub-title learndash-circle-progress-widget-circle-subtitle-style">
					<?php 
			if($settings['link_subtitle'] !== Null){
				echo '<a href="' . $settings['link_subtitle']['url'] . '"' . $target . $nofollow . '> <h3>' . $settings['subtitle'] . '</h3></a>';
			}
			else{
				echo '<h3>' . $settings['subtitle'] . '</h3>'; 
			}
			?>
				</div> 	
			</div>
			<?php } 
					/**
					 * end of text bottom container
					*/
					?>
			</div>
		<!--   end of widget container --> 
		 </div>