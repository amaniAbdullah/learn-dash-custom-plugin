<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
 
?> 
<div class="item "> 
     <div data-link="<?php echo $course_module['module_premalink']; ?>"  class="clickable-div card-container learndash-course-grid-widget-box-style">
          <div class="media-container <?php if($settings['show_media'] != 'yes'){ echo 'hide-visibility-content'; }?> ">
            <img src="<?php echo $course_module['image_url']; ?>" alt="<?php echo $course_module['lesson_id']; ?>">
            </div>   
            <div class="box-back"></div>
            <div class="icon-container learndash-course-grid-widget-overlay-style"> 
                <div class="icon <?php if($settings['show_icon'] != 'yes'){ echo 'hide-visibility-content'; }?> ">
                    <?php 
                    if($course_module['post_status'] == "publish"){
                            if($course_module['status'] == "completed"){
                                ?>
                    <span class="learndash-course-grid-widget-icon-style completed">
                    <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_completed'], [ 'aria-hidden' => 'true' ] ); ?>  
                    </span>
                    <?php
                    }
                        elseif($course_module['status'] == "notcompleted"){
                        ?>
                    <span class="learndash-course-grid-widget-icon-style incomplete">
                    <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_incomplete'], [ 'aria-hidden' => 'true' ] ); ?>  
                    </span>

                    <?php }
                        }
                        else{ 
                    ?>
                    <span class="learndash-course-grid-widget-icon-style closed ">
                    <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_closed'], [ 'aria-hidden' => 'true' ] ); ?>  
                    </span>
                        <?php } 

                    ?> 
                </div>
            </div>
            <div class="progress-content-container">
                <div class="circle-progress <?php if($settings['show_progress_bar'] != 'yes'){ echo 'hide-visibility-content'; }?>  <?php echo $settings['progress_position']; ?> learndash-course-grid-widget-circle-progress-style learndash-course-grid-widget-progress-style">
                    <div class="ProgressBar ProgressBar--animateNone" data-animation-duration="2000" data-progress="<?php if($course_module['steps'] != NULL){echo $course_module['steps'];} else{ echo '0';} ?>">
                        <svg class="ProgressBar-contentCircle"  viewBox="-4 -4 220 220"> 
                            <circle transform="rotate(-90, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="95" />    
                            <circle transform="rotate(-90, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="95" />
                            <span class="ProgressBar-percentage ProgressBar-percentage--count <?php if($settings['progress_show_value'] != 'yes'){ echo 'hide-visibility-content'; }?>"></span>
                        </svg>
                    </div> 
                </div>
                <div class="content">
                <a href="<?php echo $course_module['module_premalink']; ?>" class="card-link">
                    <div class="title <?php if($settings['show_post_title'] != 'yes'){ echo 'hide-content'; }?> "><h3 class="learndash-course-grid-widget-title-style"> <?php echo $course_module['module_title']; ?></h3></div>
                     <div class="excerpt <?php if($settings['show_excerpt'] != 'yes'){ echo 'hide-content'; }?>"><p class="learndash-course-grid-widget-excerpt-style"> <?php echo $short_excerpt; ?>...</p></div>    
                </a> 
                     <div class="link <?php if($settings['show_start_btn'] != 'yes'){ echo 'hide-content'; }?>"><a class="learndash-course-grid-widget-link-style" href="<?php echo $course_module['module_premalink']; ?>"><?php echo __('jetzt ansehen');?></a></div>
                </div>
            </div>
      </div>
                        </a>
</div>