<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
		<div class="learndash-circle-progress-widget">
		<div class="main-progress-circle-widget-container">
	  <#
		if(settings.label_position  === 'top'){ #>
		<div class="text-container">
		<div class="main-title learndash-circle-progress-widget-circle-title-style">
		 <h2>  {{ settings.title }} </h2>

		</div>
		<div class="sub-title learndash-circle-progress-widget-circle-subtitle-style">
		<#
		if(settings.link_subtitle  !== null){ #>
		 <a href="{{settings.link_subtitle.url}} "{{target}} {{nofollow}} >
			 <h3>{{settings.subtitle}}</h3></a>
		<# }
		else{ #>
			 <h3>{{settings.subtitle}}</h3>
		<# }
		#>
		</div>
		</div>
		<# } #>

		<!-- start of circle container -->

		<div class="learndash-circle-progress-hidden-background" id="hidden_back_color"></div>
		<div class="circle-conteiner">
			<div class="circle-progress ">
                    <div class="ProgressBar ProgressBar--animateNone"
					data-progress="<#
									if ( 'yes' === settings.set_value) { #>
										 {{settings.value_number_prefix}} 	<#
										}
									else { #> {{course_progress.user_course_progress}} <# }
								#>  ">

                        <svg class="ProgressBar-contentCircle learndash-circle-progress-widget-circle-style"  viewBox="-21 -21 246 246">
                            <defs class="learndash-circle-progress-back-stroke-style">
						<linearGradient id="Gradient1">
							<stop class="stop-1" offset="0%"/>
							<stop class="stop-2" offset="100%"/>
						</linearGradient>
						<linearGradient id="Gradient2">
							<stop class="stop-1" offset="0%"/>
							<stop class="stop-2" offset="100%"/>
						</linearGradient>
						</defs>
					     	<circle style="stroke:url(#Gradient1);" transform="rotate(-90, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="{{settings.stroke_back_position.size}}" />
							<circle style="animation: dash {{settings.progress_animation_duration.size}}s ease-in-out;stroke:url(#Gradient2); stroke-linecap: {{settings.value_stroke_linecap}};   -moz-animation:  dash {{settings.progress_animation_duration.size}}s ease-in-out; -webkit-animation: dash {{settings.progress_animation_duration.size}}s ease-in-out;" transform="rotate(-90, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="{{settings.value_stroke_position.size}}" />
							<span class="ProgressBar-percentage learndash-circle-progress-widget-circle-text-style">
								<#
								if(settings.show_value === 'yes'){
									if ( 'yes' === settings.set_value) { #>
										<span class="prefix">{{settings.value_number_prefix}}</span>
										<span class="suffix">{{settings.value_number_suffix}} </span>
									<#
								}
									else { #>
										<span class="prefix">{{course_progress.user_course_progress}}</span>
										<span class="suffix">% </span>

									<# }
									}
								#>
							</span>
						</svg>
                    </div>
		</div>
			</div>
		<!-- end of circle container -->
		<#
			if( settings.label_position === "default"){ #>
		<div class="text-container">
		<div class="main-title learndash-circle-progress-widget-circle-title-style">
		 <h2>  {{ settings.title }} </h2>

		</div>
		<div class="sub-title learndash-circle-progress-widget-circle-subtitle-style">
		<#
		if(settings.link_subtitle  !== null){ #>
		 <a href="{{settings.link_subtitle.url}} "{{target}} {{nofollow}} >
			 <h3>{{settings.subtitle}}</h3></a>
		<# }
		else{ #>
			 <h3>{{settings.subtitle}}</h3>
		<# }
		#>
		</div>
		</div>
		<# } #>
	 </div>
	</div>