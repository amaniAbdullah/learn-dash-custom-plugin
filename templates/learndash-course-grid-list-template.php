<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
 
?> 
<div class="item ">
     <div  class="card-container learndash-course-grid-widget-box-style"> 
     <div class="box-back"></div>
        <div class="media-icon-content">
            <div class="media-container <?php if($settings['show_media'] != 'yes'){ echo 'hide-visibility-content'; }?> ">
                <img src="<?php echo $course_module['image_url']; ?>" alt="<?php echo $course_module['lesson_id']; ?>">
            </div>   
            <div class="icon-container learndash-course-grid-widget-overlay-style"> 
                    <div class="icon <?php if($settings['show_icon'] != 'yes'){ echo 'hide-visibility-content'; }?> ">
                        <?php 
                        if($course_module['post_status'] == "publish"){
                                if($course_module['status'] == "completed"){
                                    ?>
                        <span class="learndash-course-grid-widget-icon-style completed">
                        <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_completed'], [ 'aria-hidden' => 'true' ] ); ?>  
                        </span>
                        <?php
                        }
                            elseif($course_module['status'] == "notcompleted"){
                            ?>
                        <span class="learndash-course-grid-widget-icon-style incomplete">
                        <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_incomplete'], [ 'aria-hidden' => 'true' ] ); ?>  
                        </span>

                        <?php }
                            }
                            else{ 
                        ?>
                        <span class="learndash-course-grid-widget-icon-style closed ">
                        <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_closed'], [ 'aria-hidden' => 'true' ] ); ?>  
                        </span>
                            <?php } 

                        ?> 
                </div>
            </div>
        </div> 
          <div class="progress-content-container">
                 <div class="line-progress-bar <?php if($settings['show_progress_bar'] != 'yes'){ echo 'hide-visibility-content'; }?> ">
                      <div class="ProgressBar ProgressBar--animateNone" > 
                            <div class="ProgressBar-percentage  <?php if($settings['progress_show_value'] != 'yes'){ echo 'hide-visibility-content'; }?>">
                            <?php if($course_module['steps'] != NULL){echo $course_module['steps'];} else{ echo '0';} ?>%
                            </div>
                        <div class="progress-line">
                            <div class="progress-back ProgressBar-background">
                                <div class="progress-strocke ProgressBar-circle" style="width:<?php if($course_module['steps'] != NULL){echo $course_module['steps'];} else{ echo '0';} ?>%;">
                                </div>
                            </div>
                        </div> 
                    </div> 
             </div>
                <div class="content">
                    <a href="<?php echo $course_module['module_premalink']; ?>" class="card-link">
                        <div class="title <?php if($settings['show_post_title'] != 'yes'){ echo 'hide-content'; }?> "><h3 class="learndash-course-grid-widget-title-style"> <?php echo $course_module['module_title']; ?></h3></div>
                        <div class="excerpt <?php if($settings['show_excerpt'] != 'yes'){ echo 'hide-content'; }?>"><p class="learndash-course-grid-widget-excerpt-style"> <?php echo $short_excerpt; ?>...</p></div>    
                    </a> 
                    <div class="link <?php if($settings['show_start_btn'] != 'yes'){ echo 'hide-content'; }?>"><a class="learndash-course-grid-widget-link-style" href="<?php echo $course_module['module_premalink']; ?>"><?php echo __('jetzt ansehen');?></a></div>
                </div>
           </div>
      </div>
</div>