<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
 
?>
<div class="item {{settings.layout_style}}    <# if(settings.box_height == 'fit'){#> full-screen-height <#} #>">
        <div class="card-container learndash-lasson-continue-widget-box-style">
            <# if(settings.layout_style == 'left-layout' || settings.layout_style =='right-layout'){ #>
        <div class="flex-box-content">       
            <# } #>
              <div class="media-container <# if(settings.show_media != 'yes'){ #> hide-visibility-content <# } #> ">  
                    <img src="{{course_module.image_url}}" alt="{{course_module.lesson_id}}">
                </div>   
                <div class="box-back"></div>
                <div class="icon-container learndash-lasson-continue-widget-overlay-style">
                    <div class="icon <# if(settings.show_icon != 'yes'){ #> hide-visibility-content <# } #>  ">
                    
                        <span class="learndash-lasson-continue-widget-icon-style selected">
                            {{{selectedIcon.value}}} 
                        </span>
                        
                    </div>
                </div> 
        <# if(settings.layout_style == 'left-layout' || settings.layout_style =='right-layout'){ #>
        </div>
            <div class="flex-box-content">       
            <# } #>     
            <div class="progress-content-container ">
                <div class="circle-progress <# if(settings.show_progress_bar != 'yes'){ #> hide-visibility-content <# } #>  {{settings.progress_position}} learndash-lasson-continue-widget-circle-progress-style learndash-lasson-continue-widget-progress-style">
                    <div class="ProgressBar ProgressBar--animateNone" data-animation-duration="2000" data-progress="<# if (course_module.steps != null){ #>{{course_module.steps}}<# } else { #>0<# } #>">
                        <svg class="ProgressBar-contentCircle"  viewBox="-4 -4 220 220">
                            <circle transform="rotate(-90, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="95" />    
                            <circle transform="rotate(-90, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="95" />
                            <span class="ProgressBar-percentage ProgressBar-percentage--count <# if(settings.progress_show_value != 'yes'){ #> hide-visibility-content <# } #>  "></span>
                        </svg>
                    </div>
                </div>
                <div class="content">
                <a href=" {{course_module.module_premalink}} " class="card-link">
                    <div class="title <# if(settings.show_course_title != 'yes'){ #>  hide-content <# } #>"><h2 class="learndash-lasson-continue-widget-main-title-style">   {{course_module.course_name}} </h2></div>
                    <div class="title <# if(settings.show_post_title != 'yes'){ #>  hide-content <# } #>"><h3 class="learndash-lasson-continue-widget-title-style">   {{course_module.module_title}} </h3></div>
                    <div class="excerpt <# if(settings.show_excerpt != 'yes'){ #> hide-content <# } #>"><p class="learndash-lasson-continue-widget-excerpt-style"> <# if (short_excerpt != null) { #>{{short_excerpt}}<# } #></p></div>
                 </a>
              <div class="link <# if(settings.show_start_btn  != 'yes'){ #>  hide-content <# } #>"><a class="learndash-lasson-continue-widget-link-style" href=" {{course_module.module_premalink}} "> 
                  <#
                  if(settings.button_text != null){
                      #>
                      {{settings.button_text}}
                      <#
                  }else{
                      #>
                      jetzt Ansehen
                      <#
                  } #>
                   </a></div>
               
                </div>
            </div>
            <# if(settings.layout_style == 'left-layout' || settings.layout_style =='right-layout'){ #>
            </div>      
            <# } #>
        </div> 
</div>
