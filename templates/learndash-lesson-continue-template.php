<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div data-link="<?php
 if($course_module['module_premalink'] != NULL){
    echo $course_module['module_premalink'];
 }
 else{
    echo $settings['empty_button_link'];
 }
?>" class="clickable-div item <?php echo $settings['layout_style'].' '; if($settings['box_height'] == 'fit'){ echo 'full-screen-height'; }?>">
     <div class="card-container learndash-lasson-continue-widget-box-style">
     <?php if($settings['layout_style'] == 'left-layout' || $settings['layout_style'] =='right-layout'){ ?>
        <div class="flex-box-content">
            <?php } ?>
        <div class="media-container <?php if($settings['show_media'] != 'yes'){ echo 'hide-visibility-content'; }?> ">
            <img src="<?php  if($course_module['image_url'] != NULL){ echo $course_module['image_url']; }
            else{ echo $settings['empty_image']['url']; } ?>"
            alt="<?php echo $course_module['lesson_id']; ?>">
          </div>
            <div class="box-back"></div>
            <div class="icon-container learndash-lasson-continue-widget-overlay-style">
                <div class="icon <?php if($settings['show_icon'] != 'yes'){ echo 'hide-visibility-content'; }?> ">
                    <span class="learndash-lasson-continue-widget-icon-style selected">
                    <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon'], [ 'aria-hidden' => 'true' ] ); ?>
                    </span>
                </div>
            </div>
            <?php if($settings['layout_style'] == 'left-layout' || $settings['layout_style'] =='right-layout'){ ?>
            </div>
                <div class="flex-box-content">
            <?php } ?>
            <div class="progress-content-container">

                <div style=" <?php if($course_module['steps'] == NULL){ echo "visibility:hidden;"; }?>"
                class="circle-progress <?php if($settings['show_progress_bar'] != 'yes'){ echo 'hide-visibility-content'; }?>  <?php echo $settings['progress_position']; ?> learndash-lasson-continue-widget-circle-progress-style learndash-lasson-continue-widget-progress-style">
                    <div class="ProgressBar ProgressBar--animateNone" data-animation-duration="2000" data-progress="<?php if($course_module['steps'] != NULL){echo $course_module['steps'];} else{ echo '0';} ?>">
                        <svg class="ProgressBar-contentCircle"  viewBox="-4 -4 220 220">
                            <circle transform="rotate(-90, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="95" />
                            <circle transform="rotate(-90, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="95" />
                            <span class="ProgressBar-percentage ProgressBar-percentage--count <?php if($settings['progress_show_value'] != 'yes'){ echo 'hide-visibility-content'; }?>"></span>
                        </svg>
                    </div>
                </div>
                <div class="content">
                <a href="<?php echo $course_module['module_premalink']; ?>" class="card-link">
                <div class="title <?php if($settings['show_course_title'] != 'yes'){ echo 'hide-content'; }?> ">
                <h2 class="learndash-lasson-continue-widget-main-title-style">
                    <?php echo $course_module['course_name']; ?>
                </h2>
            </div>
                    <div class="title <?php if($settings['show_post_title'] != 'yes'){ echo 'hide-content'; }?> ">
                    <h3 class="learndash-lasson-continue-widget-title-style">
                         <?php
                         if($course_module['module_title'] != NULL){
                            echo $course_module['module_title'];}
                            else{
                                echo $settings['empty_title'];
                            } ?>
                        </h3>
                    </div>
                     <div class="excerpt <?php if($settings['show_excerpt'] != 'yes'){ echo 'hide-content'; }?>">
                     <p class="learndash-lasson-continue-widget-excerpt-style">
                         <?php if ($short_excerpt != NULL){echo $short_excerpt.'..'; }?>
                        </p>
                    </div>
                </a>
                     <div class="link <?php if($settings['show_start_btn'] != 'yes'){ echo 'hide-content'; }?>">
                     <a class="learndash-lasson-continue-widget-link-style"
                     href="<?php
                     if($course_module['module_premalink'] != NULL){
                        echo $course_module['module_premalink'];
                     }
                     else{
                        echo $settings['empty_button_link'];
                     }
                        ?>">
                     <?php
                     if($course_module['module_premalink'] != NULL){
                         echo $settings['button_text'];
                     }
                     else{
                     echo $settings['empty_button'];
                     }
                     ?></a></div>
                </div>
            </div>
            <?php if($settings['layout_style'] == 'left-layout' || $settings['layout_style'] =='right-layout'){ ?>
            </div>
            <?php } ?>
      </div>
            </div>