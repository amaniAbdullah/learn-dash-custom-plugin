<?php
namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>
<div class="learndash-duration-overview-widget">
    <div class="title-value-container">
        <div class="flex-container">
            <div class="title">
                <h3 class="learndash-duration-overview-title-text">
                <?php if ($settings['title']) {echo $settings['title'];}?>
                </h3>
                <div class="icon-tooltip learndash-duration-overview-widget-icon-style">
                <?php \Elementor\Icons_Manager::render_icon($settings['selected_icon'], ['aria-hidden' => 'true']);?>

                    <div class="tooltip"><?php if ($settings['tooltip']) {echo $settings['tooltip'];}?></div>
                </div>
            </div>
            <div class="value learndash-duration-overview-value-text">
                <?php
              if ($taskLeftDays > 0) {
                       if($taskLeftDays == 1){
                        echo $taskLeftDays.' Tag';
                       }
                       else{
                        echo $taskLeftDays.' Tage';
                       }
                }
              ?>
            </div>
        </div>
    </div>

    <div class="progress-bar-container  <?php if ($precent >= 100) {echo 'hide-content';}?>">
        <div class="progress-bar-background learndash-duration-overview-widget-bar-style <?php echo $rounded_corner; ?> ">
         <div class="progress-bar-value learndash-duration-overview-widget-value-style <?php echo $rounded_corner; ?>"
         style="width:<?php
         if ($precent > 0) {
             if($precent > 100) {
                 echo "100%;";
                 }
            else{
                echo $precent . '%;';
                }
            }
        else {
            echo '0%;';
        }?>
        animation-duration: <?php echo $settings['progress_animation_duration']['size'];?>s"></div>
        </div>
    </div>
    <div class="finish-text-container  <?php if ($precent < 100  ) {echo 'hide-content';}?>">
        <h4 class="learndash-duration-overview-finish-text"><?php if ($settings['finish_text']) {echo $settings['finish_text'];}?></h4>
    </div>
</div>
