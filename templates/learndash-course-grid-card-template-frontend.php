<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
 
?>
<div class="item ">
        <div class="card-container learndash-course-grid-widget-box-style">
            <div class="media-container <# if(settings.show_media != 'yes'){ #> hide-visibility-content <# } #> ">  
                <img src="{{course_module.image_url}}" alt="{{course_module.lesson_id}}">
            </div>   
            <div class="box-back"></div>
            <div class="icon-container learndash-course-grid-widget-overlay-style">
                <div class="icon <# if(settings.show_icon != 'yes'){ #> hide-visibility-content <# } #>  ">
                    <# 
                        if(course_module.post_status == "publish"){
                            if(course_module.status == "completed"){#>
                    <span class="learndash-course-grid-widget-icon-style completed">
                        {{{iconCompleted.value}}} 
                    </span>
                    <#
                            }
                            else if(course_module.status == "notcompleted"){ #>
                    <span class="learndash-course-grid-widget-icon-style incomplete">
                        {{{iconIncompleted.value}}} 
                    </span> 
                            <# }
                        }
                        else{ #>
                    <span class="learndash-course-grid-widget-icon-style closed">
                        {{{iconClosed.value}}}
                    </span>
                        <# }  
                        #> 
                </div>
            </div>
            <div class="progress-content-container ">
                <div class="circle-progress <# if(settings.show_progress_bar != 'yes'){ #> hide-visibility-content <# } #>  {{settings.progress_position}} learndash-course-grid-widget-circle-progress-style learndash-course-grid-widget-progress-style">
                    <div class="ProgressBar ProgressBar--animateNone" data-animation-duration="2000" data-progress="<# if (course_module.steps != null){ #>{{course_module.steps}}<# } else { #>0<# } #>">
                        <svg class="ProgressBar-contentCircle"  viewBox="-4 -4 220 220">
                            <circle transform="rotate(-90, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="95" />    
                            <circle transform="rotate(-90, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="95" />
                            <span class="ProgressBar-percentage ProgressBar-percentage--count <# if(settings.progress_show_value != 'yes'){ #> hide-visibility-content <# } #>  "></span>
                        </svg>
                    </div>
                </div>
                <div class="content">
                <a href=" {{course_module.module_premalink}} " class="card-link">
                    <div class="title <# if(settings.show_post_title != 'yes'){ #>  hide-content <# } #>"><h3 class="learndash-course-grid-widget-title-style">   {{course_module.module_title}} </h3></div>
                    <div class="excerpt <# if(settings.show_excerpt != 'yes'){ #> hide-content <# } #>"><p class="learndash-course-grid-widget-excerpt-style"> {{short_excerpt}}...</p></div>
                 </a>
              <div class="link <# if(settings.show_start_btn  != 'yes'){ #>  hide-content <# } #>"><a class="learndash-course-grid-widget-link-style" href=" {{course_module.module_premalink}} "> jetzt ansehen </a></div>
               
                </div>
            </div>
        </div> 
</div>