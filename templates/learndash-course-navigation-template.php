<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="learndash-course-navigation-widget">
    <?php
     foreach($breadcrumbs as $breadcrumb){
      if($breadcrumb['course_id']== $course_id){
        ?>
    <div class="breadcrumbs-container <?php if($settings['show_breadcrumbs']!="yes"){  echo 'hide-content'; }?>">
        <ul>
            <?php
                if($settings['custom_label_breadcrumbs'] == "yes"){ ?>
            <li class="widget-course-navigation-breadcrumbs-style "><?php
                if($settings['link_breadcrumbs']['url'] != NULL){
                    echo '<a href="' . $settings['link_breadcrumbs']['url'] . '"' . $target . $nofollow . '>'.$settings['custom_label_breadcrumbs_text'].' > </a>';
                    } else{
                        echo $settings['custom_label_breadcrumbs_text'].' >';
                    }
                ?></li>
            <?php }
                    ?>
            <li class="widget-course-navigation-breadcrumbs-style">
                <a href="<?php echo $breadcrumb['course_url']; ?>">
                    <?php echo $breadcrumb['course_name'].'.. >'; ?>
                </a>
            </li>
            <li class="widget-course-navigation-breadcrumbs-style <?php if($breadcrumb['topic_name'] == NULL){ echo 'active'; }?>">
                <a href="<?php echo $breadcrumb['lesson_url']; ?>">
                    <?php echo $breadcrumb['lesson_name'].'.. >'; ?>
                </a>
            </li>
            <?php if($breadcrumb['topic_name'] != NULL){ ?>
            <li class="widget-course-navigation-breadcrumbs-style active">
                <a href="<?php echo $breadcrumb['topic_url']; ?>">
                    <?php echo $breadcrumb['topic_name'].'..'; ?>
                </a>
            </li>
            <?PHP } ?>
        </ul>
    </div>
    <?php
        } }
        $rand= rand();
        ?>
    <div class="accordion-option accordion" id="<?php echo $rand.'-accordion'; ?>">
        <?php

        $x=0;
        foreach ($course_modules as $course_module){
            if($course_module['course_id']== $course_id){
        ?>
        <div class="ac-container">
            <div class="ac-title <?php if($breadcrumb['lesson_id'] == $course_module['module_id']){ echo 'open-collapse';}?>" id="<?php echo $rand.'-'.$x.'-title'; ?>">
                <span class="widget-course-navigation-icon-collapse-style collapse">
                    <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_collapse'], [ 'aria-hidden' => 'true' ] ); ?>
                </span>
                <span class="widget-course-navigation-lessons-style">
                    <a href="<?php echo $course_module['module_URL']; ?>">
                        <?php echo $course_module['module_title'];?> </a>
                </span>
                <span class="progress-content-container">
                <?php if($course_module['steps'] == 100) { ?>
                <span class="widget-course-navigation-icon-course-completed-style course-completed">
                    <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_course_completed'], [ 'aria-hidden' => 'true' ] ); ?>
                </span>
                <?php } else{?>
                    <span class="circle-progress learndash-course-navigation-widget-circle-progress-style ">
                        <span class="ProgressBar ProgressBar--animateNone" data-animation-duration="2000" data-progress="<?php if($course_module['steps'] != NULL){echo $course_module['steps'];} else{ echo '0';} ?>">
                            <svg class="ProgressBar-contentCircle" viewBox="-4 -4 220 220">
                                <circle transform="rotate(-90, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="95" />
                                <circle transform="rotate(-90, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="95" />
                                <span class="ProgressBar-percentage ProgressBar-percentage--count hide-visibility-content"></span>
                            </svg>
                        </span>
                        <?php }?>
                    </span>
                </span>
            </div>
            <div class="ac-content <?php if($breadcrumb['lesson_id'] == $course_module['module_id']){ echo 'show-content';} else{ echo 'hide-content'; }?> " id="<?php echo $rand.'-'.$x.'-content'; ?>">
                <ul>
                    <?php
                        $i=0;
                        foreach($course_modules_lessons as $course_modules_lesson){
            if($course_modules_lesson['course_id']== $course_id && $course_modules_lesson['module_id'] ==  $course_module['module_id']){
                    if($course_modules_lesson['topic_user_status'] == 'complete'){
                        ?>
                    <li>
                        <a class=" widget-course-navigation-topics-style border widget-course-navigation-topics-background <?php  if($course_modules_lesson['topic_id'] == $course_modules_lesson['current_page'] ){ echo 'active';} ?>" href='<?php  echo $course_modules_lesson['topic_premalink']; ?>'>
                            <span class="widget-course-navigation-topics-style-text"><?php echo $course_modules_lesson['topic_title']; ?></span>
                            <span class="widget-course-navigation-icon-style completed <?php  if($course_modules_lesson['topic_id'] == $course_modules_lesson['current_page'] ){ echo 'active';} ?>">
                                <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_completed'], [ 'aria-hidden' => 'true' ] ); ?>
                            </span>
                        </a>
                    </li>
                    <?php }
                        else{
                        ?>
                    <li>
                        <a class=" widget-course-navigation-topics-style border widget-course-navigation-topics-background <?php  if($course_modules_lesson['topic_id'] == $course_modules_lesson['current_page'] ){ echo 'active';} ?>" href='<?php echo $course_modules_lesson['topic_premalink']?>'>
                            <span class="widget-course-navigation-topics-style-text"><?php echo $course_modules_lesson['topic_title']; ?></span>
                            <span class="widget-course-navigation-icon-style incomplete <?php  if($course_modules_lesson['topic_id'] == $course_modules_lesson['current_page'] ){ echo 'active';} ?>">
                                <?php \Elementor\Icons_Manager::render_icon( $settings['selected_icon_incomplete'], [ 'aria-hidden' => 'true' ] ); ?>
                            </span>
                        </a>
                    </li>
                    <?php }
                     ?>
                    <?php

                          }
                $i++; }
                        ?>
                </ul>
            </div>
        </div>
        <?php
              }
              $x++;   }
            ?>
    </div>
</div>
