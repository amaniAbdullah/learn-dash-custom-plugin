<?php

/**
 * Plugin Name:       Elementor Custom Widgets for learndash
 * Description:       This plugin adds new widgets for Elementor integrated with Learndash plugin and BuddyBoss theme
 * Version:           2.0.0
 * Requires at least: 5.8.1
 * Requires PHP:      7.3.4
 * Author:            Tessafold Company
 * Author URI:        http://tessafold.com/
 * License:           GPL v2 or later
 * Text Domain:       elementor-custom-widgets-for-learndash
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Domain Path:       /languages
 */

//Prevent anyone to access plugin code
if(!defined('ABSPATH')){
    die ("You can't access this file");
}

/**
 * Elementor widgets main class
 * @since 2.0.0
 */
final class ELEMENTOR_LEARNDASH_CUSTOM_WIDGETS{
    //Plugin Version
    const VERSION ='2.0.0';

    //Minimum Elementor Version
    const MINIMUM_ELEMENTOR_VERSION = '3.2.4';

    //Minimum Php Version
    const MINIMUM_PHP_VERSION = '7.3.4';

    //Instance
    private static $_instance = null;

    /**
     * Singletone Instance Method
     * @since 1.0.0
     */

     public static function instance(){
         if (is_null(self::$_instance)){
             self::$_instance = new self();
         }
         return self::$_instance;

     }

     /**
      * Construct Method
      * @since 2.0.0
      */
      public function __construct() {
          //Call Constants Method
          $this->define_constants();
            add_action('wp_enqueue_scripts', [$this,'scripts_styles']);
          add_action('init', [$this, 'i18n']);
        add_action( 'plugins_loaded', [ $this, 'init' ] );


      }

      /**
       * Define Plugin Constants
       * @since 1.0.0
       */
      public function define_constants(){
          define('ELCW_PLUGIN_URL', trailingslashit(plugins_url('/',__FILE__)));
          define('ELCW_PLUGIN_PATH', trailingslashit(plugin_dir_path(__FILE__)));
          define( 'ELCW_PLUGIN_PATH_template', dirname( __FILE__ ) . '/templates' );
          define( 'ELCW_PLUGIN_PATH_image', trailingslashit(plugins_url( '/',__FILE__ ) . 'assets/source/images') );
      }


      /**
       * Load Scripts and Styles
       * @since 1.0.0
       */
       public function scripts_styles(){

        wp_register_style( 'elcw-owl-carousel', ELCW_PLUGIN_URL . 'assets/vendor/owl-carousel/css/owl.carousel.min.css',  [ 'elementor-frontend' ], rand(), 'all' );
        wp_register_style( 'elcw-owl-carousel-theme', ELCW_PLUGIN_URL . 'assets/vendor/owl-carousel/css/owl.theme.default.min.css',  [ 'elementor-frontend' ], rand(), 'all' );
        wp_register_script( 'elcw-owl-carousel', ELCW_PLUGIN_URL . 'assets/vendor/owl-carousel/js/owl.carousel.min.js', [ 'jquery' ], rand(), true );
        wp_register_style('elcw-style',ELCW_PLUGIN_URL.'assets/dist/css/public.min.css',  [ 'elementor-frontend' ], rand(), 'all' );
        wp_register_script('elcw-script',ELCW_PLUGIN_URL. 'assets/dist/js/public.min.js', [ 'jquery' ],  [ 'elementor-frontend' ], rand(), true );

        wp_enqueue_style( 'elcw-owl-carousel' );
        wp_enqueue_style( 'elcw-owl-carousel-theme' );
        wp_enqueue_script( 'elcw-owl-carousel' );
        wp_enqueue_style( 'elcw-style' );
        wp_enqueue_script( 'elcw-script' );

       }

      /**
       * Load Text Domain
       * @since 1.0.0
       */
      public function i18n(){
          load_plugin_textdomain('elementor-custom-widgets-for-learndash', false , dirname(plugin_basename(__FILE__)) . '/languages');
      }

      /**
       * Initialize the Plugin
       * @since 1.0.0
       */

       public function init(){
        // Check if the ELementor installed and activated
        if( ! did_action( 'elementor/loaded' ) ) {
            add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
            return;
        }
        if( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
            add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
            return;
        }

        if( ! version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '>=' ) ) {
            add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
            return;
        }
        add_action('elementor/init',[$this, 'init_category']);
        add_action('elementor/widgets/widgets_registered',[$this, 'init_widgets']);
       }

       /**
        * Initialize the widgets
        */

        public function init_widgets(){
            require_once ELCW_PLUGIN_PATH .'/widgets/learndash-circle-progress-widget.php';
            require_once ELCW_PLUGIN_PATH .'/widgets/learndash-course-grid-widget.php';
            require_once ELCW_PLUGIN_PATH .'/widgets/learndash-course-navigation-widget.php';
            require_once ELCW_PLUGIN_PATH .'/widgets/learndash-lesson-continue-widget.php';
            require_once ELCW_PLUGIN_PATH .'/widgets/learndash-zoom-announcement-widget.php';
            require_once ELCW_PLUGIN_PATH .'/widgets/learndash-duration-overview-widget.php';
            require_once ELCW_PLUGIN_PATH .'/widgets/learndash-button.php';
        }

        /**
         * Init Category Section
         * @since 1.0.0
         */
        public function init_category(){
            Elementor\Plugin::instance()->elements_manager->add_category(
                'learndash-custom-widgets-for-elementor',
                [
                    'title'=>'Learndash Custom Widgets'
                ],
                1
                );
        }
        /**
         * Adding notice when the site doesn't have elementor installed
         * @since 1.0.0
         */
        public function admin_notice_missing_main_plugin() {
            if( isset( $_GET[ 'activate' ] ) ) unset( $_GET[ 'activate' ] );
            $message = sprintf(
                esc_html__( '"%1$s" requires "%2$s" to be installed and activated', 'elementor-custom-widgets-for-learndash' ),
                '<strong>'.esc_html__( 'Elementor Custom Widgets for learndash ', 'elementor-custom-widgets-for-learndash' ).'</strong>',
                '<strong>'.esc_html__( 'Elementor', 'elementor-custom-widgets-for-learndash' ).'</strong>'
            );

            printf( '<div class="notice notice-warning is-dimissible"><p>%1$s</p></div>', $message );
        }
            /**
    * Admin Notice
    * Warning when the site doesn't have a minimum required Elementor version.
    * @since 1.0.0
    */
    public function admin_notice_minimum_elementor_version() {
        if( isset( $_GET[ 'activate' ] ) ) unset( $_GET[ 'activate' ] );
        $message = sprintf(
            esc_html__( '"%1$s" requires "%2$s" version %3$s or greater', 'elementor-custom-widgets-for-learndash' ),
            '<strong>'.esc_html__( 'Elementor Custom Widgets for learndash', 'elementor-custom-widgets-for-learndash' ).'</strong>',
            '<strong>'.esc_html__( 'Elementor', 'elementor-custom-widgets-for-learndash' ).'</strong>',
            self::MINIMUM_ELEMENTOR_VERSION
        );

        printf( '<div class="notice notice-warning is-dimissible"><p>%1$s</p></div>', $message );
    }

    /**
    * Admin Notice
    * Warning when the site doesn't have a minimum required PHP version.
    * @since 1.0.0
    */
    public function admin_notice_minimum_php_version() {
        if( isset( $_GET[ 'activate' ] ) ) unset( $_GET[ 'activate' ] );
        $message = sprintf(
            esc_html__( '"%1$s" requires "%2$s" version %3$s or greater', 'elementor-custom-widgets-for-learndash' ),
            '<strong>'.esc_html__( 'Elementor Custom Widgets for learndash', 'elementor-custom-widgets-for-learndash' ).'</strong>',
            '<strong>'.esc_html__( 'PHP', 'elementor-custom-widgets-for-learndash' ).'</strong>',
            self::MINIMUM_PHP_VERSION
        );

        printf( '<div class="notice notice-warning is-dimissible"><p>%1$s</p></div>', $message );
    }





}

ELEMENTOR_LEARNDASH_CUSTOM_WIDGETS::instance();
require plugin_dir_path( __FILE__ ) . 'includes/class-ld-dependency-check.php';
LearnDash_Dependency_Check_LD_Elementor::get_instance()->set_dependencies(
	array(
		'sfwd-lms/sfwd_lms.php'           => array(
			'label'       => '<a href="https://learndash.com">LearnDash LMS</a>',
			'class'       => 'SFWD_LMS',
			'min_version' => '3.1.6',
		),
		'elementor/elementor.php'         => array(
			'label'       => '<a href="https://elementor.com">Elementor</a>',
			'min_version' => '2.9.8',
		),
	)
);
LearnDash_Dependency_Check_LD_Elementor::get_instance()->set_message(
	esc_html__( 'LearnDash LMS Elementor Add-on requires the following plugin(s) be active:', 'learndash-elementor' )
);
