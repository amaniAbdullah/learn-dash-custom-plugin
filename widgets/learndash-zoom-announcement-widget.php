<?php
namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
if (!defined('ABSPATH')) {
    $parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
    include_once $parse_uri[0] . '/wp-load.php';
}

define('WP_USE_THEMES', true);

define('ELCW_PLUGIN_URL', plugin_dir_path(__FILE__));
class ZOOM_ANNOUNCEMENT_WIDGET extends Widget_Base
{
    public function get_group_avatar()
    {
        global $wpdb;
        $user_ID = get_current_user_id();
        $groups = $wpdb->get_results(" SELECT * FROM `" . $wpdb->prefix . "bp_groups`");
        $items = array();
        foreach ($groups as $group) {
            $items[] = array(
                'group_meeting_id' => $group->id,
                'group_avatar_image' => bp_core_fetch_avatar(array(
                    'item_id' => $group->id,
                    'avatar_dir' => 'group-avatars',
                    'object' => 'group',
                    'html' => false,
                    'force_default' => false,

                )),
            );
        }
        return (array) $items;
    }
    public function get_group_slug()
    {
        global $wpdb;
        $user_ID = get_current_user_id();
        $groups = $wpdb->get_results(" SELECT * FROM `" . $wpdb->prefix . "bp_groups`");
        $items = array();
        foreach ($groups as $group) {
            $items[] = array(
                'group_slug' => $group->slug,
                'group_id' => $group->id,
            );
        }
        return (array) $items;

    }
    public function get_all_groups()
    {

        global $wpdb;
        $user_ID = get_current_user_id();
        $groups = $wpdb->get_results(" SELECT * FROM `" . $wpdb->prefix . "bp_groups`");
        $items = array();
        foreach ($groups as $group) {
            $items[$group->id] = $group->name;
        }
        return (array) $items;

    }
    public function get_all_meeting()
    {
        global $wpdb;
        $user_ID = get_current_user_id();
        $groups = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "bp_zoom_meetings` WHERE `start_date` >= NOW()");
        $items = array();
        foreach ($groups as $group) {
            $date_formate = date('d.m.Y - H:i', strtotime($group->start_date));
            $post = get_post($post_id);
            $slug = $post->post_name;
            $items[] = array(
                'meeting_group_id' => $group->group_id,
                'meeting_id' => $group->id,
                'meeting_title' => $group->title,
                'meeting_date' => $date_formate,
                'meeting_url' => bp_get_group_permalink(),
            );
        }
        return (array) $items;

    }
    public function get_name()
    {
        return 'zoom-announcement-widget-id';
    }

    public function get_title()
    {
        return esc_html__('Zoom Announcement', 'elementor-custom-widgets-for-learndash');
    }
    public function get_script_depends()
    {
        return [
            'elcw-script',
        ];
    }
    public function get_icon()
    {
        return 'eicon-video-camera';
    }
    public function get_categories()
    {
        return [
            'learndash-custom-widgets-for-elementor',
        ];
    }
    public function _register_controls()
    {
        //Content Setings
        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Settings', 'elementor-custom-widgets-for-learndash'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control(
            'group_id',
            [
                'label' => __('Group', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::SELECT2,
                'multiple' => false,
                'options' => $this->get_all_groups(),
            ]
        );
        $this->add_control(
            'hidden_meetings',
            [
                'label' => __('View', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->get_all_meeting(),
            ]
        );
        $this->add_control(
            'hidden_slug',
            [
                'label' => __('View', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->get_group_slug(),
            ]
        );
        $this->add_control(
            'hidden_group_avatar_image',
            [
                'label' => __('View', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->get_group_avatar(),
            ]
        );

        $this->add_control(
            'notifications_num',
            [
                'label' => __('Notifications', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'min' => 0,
                'max' => 100,
                'step' => 1,
                'default' => 3,

            ]
        );
        $this->end_controls_section();
        //Content Empty Announcement Setting
        $this->start_controls_section(
            'empty_announcement_box',
            [
                'label' => __('Empty Announcement Box', 'elementor-custom-widgets-for-learndash'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control(
            'empty_box_title',
            [
                'label' => esc_html__('Title', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
                'default' => __('Title of empty announcement box ', 'elementor-custom-widgets-for-learndash'),
                'placeholder' => esc_html__('Title of empty announcement box', 'elementor-custom-widgets-for-learndash'),
            ]
        );
        $this->add_control(
            'empty_box_subtitle',
            [
                'label' => esc_html__('Subtitle', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
                'default' => __('Subtitle of empty announcement box', 'elementor-custom-widgets-for-learndash'),
                'placeholder' => __('Subtitle of empty announcement box', 'elementor-custom-widgets-for-learndash'),
            ]
        );

        $this->add_control(
            'empty_box_link',
            [
                'label' => esc_html__('Box Link', 'elementor'),
                'type' => Controls_Manager::URL,
                'dynamic' => [
                    'active' => true,
                ],
                'default' => [
                    'url' => '#',
                ],
                'placeholder' => __('http://dein-link.de/', 'elementor-custom-widgets-for-learndash'),

            ]
        );
        $this->end_controls_section();
        $this->start_controls_section(
            'box_tab',
            [
                'label' => __('Box', 'elementor-custom-widgets-for-learndash'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_group_control(
            \Elementor\Group_Control_Border::get_type(),
            [
                'name' => 'box_border',
                'label' => __('Border', 'elementor-custom-widgets-for-learndash'),
                'selector' => '{{WRAPPER}} .widget-zoom-announcement-box-style',
            ]
        );
        $this->add_responsive_control(
            'border_box_radius',
            [
                'label' => __('Border Radius', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%', 'em'],
                'default' => [
                    'top' => 12,
                    'right' => 12,
                    'bottom' => 12,
                    'left' => 12,
                ],
                'selectors' => [
                    '{{WRAPPER}} .widget-zoom-announcement-box-style' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->add_group_control(
            \Elementor\Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'box_shadow',
                'label' => __('Box Shadow', 'elementor-custom-widgets-for-learndash'),
                'selector' => '{{WRAPPER}} .widget-zoom-announcement-box-style',
            ]
        );
        $this->add_responsive_control(
            'box_padding',
            [
                'label' => __('Padding', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::DIMENSIONS,
                'description' => 'Max Padding: 50px',
                'size_units' => ['px'],
                'default' => [
                    'top' => 30,
                    'right' => 20,
                    'bottom' => 30,
                    'left' => 20,
                ],
                'selectors' => [
                    '{{WRAPPER}} .widget-zoom-announcement-box-style-a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]
        );
        $this->start_controls_tabs(
            'box_style_section_tabs'
        );
        // Normal State
        $this->start_controls_tab(
            'box_background_normal_state',
            [
                'label' => __('Normal', 'elementor-custom-widgets-for-learndash'),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'box_background_normal',
                'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .widget-zoom-announcement-box-style',
                'fields_options' => [
                    'background' => [
                        'default' => 'classic',
                    ],
                    'color' => [
                        'default' => 'rgba(147,1,60,1)',

                    ],
                ],
            ]
        );

        $this->end_controls_tab();

        // Hover State
        $this->start_controls_tab(
            'box_background_hover_state',
            [
                'label' => __('Hover', 'elementor-custom-widgets-for-learndash'),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'box_background_hover',
                'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .widget-zoom-announcement-box-style:hover',
                'fields_options' => [
                    'background' => [
                        'default' => 'classic',
                    ],
                    'color' => [
                        'default' => 'rgba(147,1,60,1)',
                    ],
                ],
            ]
        );
        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->add_control(
            'margin_to_next_box',
            [
                'label' => __('Margin To Next Box', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'size' => 30,
                ],
                'separator' => 'before',
                'selectors' => [
                    '{{WRAPPER}} .widget-zoom-announcement-box-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->end_controls_section();
        $this->start_controls_section(
            'media_tab',
            [
                'label' => __('Media', 'elementor-custom-widgets-for-learndash'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
            'media_size',
            [
                'label' => __('Size', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'description' => 'Default: 100px',
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 500,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 62,
                ],
                'selectors' => [
                    '{{WRAPPER}} .widget-zoom-announcement-media-style>img' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'media_padding_right',
            [
                'label' => __('Padding Right', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'description' => 'Default: 30px',
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 60,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 25,
                ],
                'selectors' => [
                    '{{WRAPPER}} .widget-zoom-announcement-media-padding-right' => 'margin-right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->end_controls_section();
        $this->start_controls_section(
            'content_tab',
            [
                'label' => __('Content', 'elementor-custom-widgets-for-learndash'),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'heading_1',
            [
                'label' => __('Zoom Title', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HEADING,
            ]
        );
        // Title Color
        $this->add_control(
            'title_color',
            [
                'label' => __('Text Color', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .widget-zoom-announcement-title-style' => 'color: {{VALUE}}',
                ],
                'default' => '#fff',
            ]
        );
        //Title Text Typography
        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'title_text_typography',
                'label' => __('Text Typography', 'elementor-custom-widgets-for-learndash'),
                'selector' => '{{WRAPPER}} .widget-zoom-announcement-title-style',
            ]
        );
        $this->add_control(
            'title_text_padding',
            [
                'label' => __('Padding', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'size' => 0,
                ],
                'selectors' => [
                    '{{WRAPPER}} .widget-zoom-announcement-title-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'heading_2',
            [
                'label' => __('Date', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        // date Color
        $this->add_control(
            'date_color',
            [
                'label' => __('Text Color', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .widget-zoom-announcement-date-style' => 'color: {{VALUE}}',
                ],
                'default' => '#fff',
            ]
        );
        //date Text Typography
        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'date_text_typography',
                'label' => __('Text Typography', 'elementor-custom-widgets-for-learndash'),
                'selector' => '{{WRAPPER}} .widget-zoom-announcement-date-style',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        global $wpdb;
        $settings = $this->get_settings_for_display();
        $meetings = $settings['hidden_meetings'];
        $groups_avatar = $settings['hidden_group_avatar_image'];
        $group_slug_vals = $settings['hidden_slug'];
		$target = $settings['empty_box_link']['is_external'] ? ' target="_blank"' : '';
		$nofollow = $settings['empty_box_link']['nofollow'] ? ' rel="nofollow"' : '';
        $dir = ELCW_PLUGIN_URL;

        ?>
          <div class="zoom-announcement-widget">
            <div class="card-container ">
                <?php
$i = 1;
        if ($meetings != null) {
            foreach ($meetings as $meeting) {
                if ($meeting['meeting_group_id'] == $settings['group_id']) {
                    if ($i <= $settings['notifications_num']) {
                        foreach ($group_slug_vals as $group_slug_val) {
                            if ($group_slug_val['group_id'] == $settings['group_id']) {
                                $meeting_url = $meeting['meeting_url'];
                                $group_slug_name = $group_slug_val['group_slug'];
                                $meeting_id = $meeting['meeting_id'];

                                ?>
                <div class="zoom-announcement-box widget-zoom-announcement-box-style">
                    <a class="widget-zoom-announcement-box-style-a" href="<?php echo $meeting_url . $group_slug_name . '/zoom/meetings/' . $meeting_id; ?>" >
                        <div class= zoom-announcement-flex-box>
                    <div class="media-container widget-zoom-announcement-media-padding-right">
                    <div class="widget-zoom-announcement-media-container widget-zoom-announcement-media-style img-responsive">
                        <?php
                        foreach ($groups_avatar as $group_avatar) {
                            if ($group_avatar['group_meeting_id'] == $settings['group_id']) {
                                        ?>
                           <img alt="<?php echo $meeting['meeting_title']; ?>" src="<?php echo $group_avatar['group_avatar_image']; ?>" >
                           <?php
                           }
                        }
                        ?>
                        </div>
                </div>
                    <div class="content-container">
                        <div class="title-container"><h3 class="widget-zoom-announcement-title-style"><?php echo $meeting['meeting_title']; ?></h3></div>
                        <div class="date-container"><span class="widget-zoom-announcement-date-style"><?php echo $meeting['meeting_date']; ?></span></div>
                    </div>
                  </div>
                    </a>
                </div>
                <?php
                }
            }
        }
        $i++;
    }
}
}
else {
    ?>
            <div class="zoom-announcement-box widget-zoom-announcement-box-style">
            <a class="widget-zoom-announcement-box-style-a" <?php if ($settings["empty_box_link"]["url"] != NULL){ ?>
                href='<?php echo $settings["empty_box_link"]["url"]; ?>' <?php echo $target . $nofollow; } else { ?>href="#"<?php  }?> >

                <div class= zoom-announcement-flex-box>
            <div class="media-container widget-zoom-announcement-media-padding-right">
            <div class="widget-zoom-announcement-media-container widget-zoom-announcement-media-style img-responsive">
                <?php
                foreach ($groups_avatar as $group_avatar) {
                    if ($group_avatar['group_meeting_id'] == $settings['group_id']) {
                    ?>
                   <img alt="group-image" src="<?php echo $group_avatar['group_avatar_image']; ?>" >
                   <?php
                    }
               }
            ?>
                </div>
        </div>
           <div class="content-container">
            <div class="title-container"><h3 class="widget-zoom-announcement-title-style"><?php echo $settings['empty_box_title']; ?></h3></div>
            <div class="date-container"><span class="widget-zoom-announcement-date-style"><?php echo $settings['empty_box_subtitle']; ?></span></div>
           </div>
          </div>
            </a>
        </div>
        <?php
}
        ?>
            </div>
        </div>
<?php
}
    protected function _content_template()
    {
        ?>
          <#
          var meetings= settings.hidden_meetings;
        var groups_avatar=  settings.hidden_group_avatar_image;
        var group_slug_vals=settings.hidden_slug;
          #>
        <div class="zoom-announcement-widget">
            <div class="card-container ">

            <div class="zoom-announcement-box widget-zoom-announcement-box-style">
                <# if(settings.empty_box_link.url !== null){ #>
                <a class="widget-zoom-announcement-box-style-a" href="{{settings.empty_box_link.url}} " >
                <# } else { #>
                    <a class="widget-zoom-announcement-box-style-a" href="{{settings.empty_box_link}} " >
                <# } #>
                <div class= zoom-announcement-flex-box>
                    <div class="media-container widget-zoom-announcement-media-padding-right">
                    <div class="widget-zoom-announcement-media-container widget-zoom-announcement-media-style img-responsive">
                        <#
                        _.each(   groups_avatar , function( group_avatar){
                            if( group_avatar.group_meeting_id  == settings.group_id){
                               #>
                               <img alt="group-title" src="{{group_avatar.group_avatar_image}}">
                               <#
                            }
                       })
                        #>
                </div>
                    </div>
                    <div class="content-container">
                    <div class="title-container"><h3 class="widget-zoom-announcement-title-style">{{settings.empty_box_title}}</h3></div>
                    <div class="date-container"><span class="widget-zoom-announcement-date-style"> {{settings.empty_box_subtitle}} </span></div> </div>
                </div>
                    </a>
                </div>
            <#
            var i =1;
            _.each( meetings , function( meeting ) {
                    if(meeting.meeting_group_id == settings.group_id){
                        if( i <= settings.notifications_num ){
                            _.each(group_slug_vals , function(group_slug_val){
                                if(group_slug_val.group_id  ==  settings.group_id){
                                 var meeting_url= meeting.meeting_url;
                                 var group_slug_name= group_slug_val.group_slug;
                                 var meeting_id = meeting.meeting_id;
                #>
                <div class="zoom-announcement-box widget-zoom-announcement-box-style">
                <a class="widget-zoom-announcement-box-style-a" href=" {{meeting_url}}{{group_slug_name}}/zoom/meetings/{{meeting_id}} " >
                      <div class= zoom-announcement-flex-box>
                    <div class="media-container widget-zoom-announcement-media-padding-right">
                    <div class="widget-zoom-announcement-media-container widget-zoom-announcement-media-style img-responsive">
                        <#
                        _.each(   groups_avatar , function( group_avatar){
                            if( group_avatar.group_meeting_id  == settings.group_id){
                               #>
                               <img alt="{{meeting.meeting_title}}" src="{{group_avatar.group_avatar_image}}">
                               <#
                            }
                       })
                        #>
                </div>
                    </div>
                    <div class="content-container">

                    <div class="title-container"><h3 class="widget-zoom-announcement-title-style">   {{meeting.meeting_title}} </h3></div>
                        <div class="date-container"><span class="widget-zoom-announcement-date-style"> {{meeting.meeting_date}} </span></div> </div>
                  </div>
                    </a>
                </div>
                <# } }) } i++; } })
                 #>

            </div>
        </div>

        <?php
}
}
Plugin::instance()->widgets_manager->register_widget_type(new ZOOM_ANNOUNCEMENT_WIDGET());