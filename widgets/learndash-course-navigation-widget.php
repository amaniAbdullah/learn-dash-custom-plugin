<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
if ( ! defined( 'ABSPATH' ) ) {
	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	include_once $parse_uri[0] . '/wp-load.php';
}

define( 'WP_USE_THEMES', true );
class LEARNDASH_COURSE_NAVIGATION_WIDGET extends Widget_Base{
    public function get_breadcrumbs(){
        global $post;
        $post_ID =  get_the_ID();
        $postType= get_post_type();
        $course_id_topic    = learndash_get_course_id();
        $topics= $this->get_course_modules_lessons();
        $lessons = $this->get_course_all_modules();
        if($postType == 'sfwd-topic'){
        foreach($topics as $topic){
          if($topic['course_id'] ==$course_id_topic  ){
                if($topic['topic_id'] == $post_ID){
                    $items[]=array(
                    'topic_id'=> get_the_ID(),
                    'lesson_id'=>$topic['module_id'],
                    'course_id'=>$topic['course_id']  ,
                    'course_url'=> get_permalink( $topic['course_id'] ),
                    'course_name'=>substr(get_the_title($topic['course_id']),0,10),
                    'lesson_url'=> learndash_get_step_permalink($topic['module_id'],$topic['course_id']),
                    'lesson_name'=>substr(get_the_title( $topic['module_id']),0,10),
                    'topic_url'=> $topic['topic_premalink'],
                    'topic_name'=>substr(get_the_title($topic['topic_id']),0,10)
                    );
               }
            }
              }
            }
              else{
                foreach($lessons as $lesson){
                  if($lesson['module_id'] == $post_ID){
                     $items[]=array(
                        'topic_id'=> '',
                        'lesson_id'=>get_the_ID(),
                        'course_id'=>$lesson['course_id']  ,
                        'course_url'=> get_permalink( $lesson['course_id'] ),
                        'course_name'=>substr(get_the_title($lesson['course_id']),0,10),
                        'lesson_url'=> $lesson['module_URL'],
                        'lesson_name'=>substr($lesson['module_title'],0,10),
                        'topic_url'=>$lesson['module_URL'],
                        'topic_name'=>''
                        );
                   }
              }

        }

    return (array) $items;

    }
    public function get_courses(){
    global $wpdb;
    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
        $items=array();
    foreach($courses as $course){
        $items[$course->ID ]= $course->post_title;
    }
    return (array) $items;

}
public function get_course_all_modules(){
    global $wpdb;
    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
    $meta= array();
    foreach($courses as $course){
        $lessons=learndash_get_course_lessons_list($course->ID);
        foreach($lessons as $lesson){
        $sections= learndash_30_get_course_sections($course->ID );
        $lesson_progress = learndash_lesson_progress( $lesson['post']->ID, $course->ID );

            if($lesson_progress['percentage'] == Null && $lesson['status'] == "completed"){
                $steps= 100;
            }
            else if($lesson_progress['percentage'] == Null && $lesson['status'] == "notcompleted"){
                $steps = 0;
            }
            else{
                $steps =$lesson_progress['percentage'];
            }
            $meta[]=array(
                    'course_id'=>$course->ID,
                    'module_id'=>$lesson['post']->ID,
                    'module_name'=>$lesson['post']->post_name,
                    'module_title'=>$lesson['post']->post_title,
                    'module_URL'=>learndash_get_step_permalink( $lesson['post']->ID, $course->ID),
                    'steps'=>$steps
                );
            }
        }
    return (array) $meta;
}
public function get_course_none_sectioned_modules(){
    global $wpdb;
    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
    $meta= array();
    foreach($courses as $course){
        $lessons=learndash_get_course_lessons_list($course->ID);
        foreach($lessons as $lesson){
        $sections= learndash_30_get_course_sections($course->ID );
            if(!isset($sections[$lesson['post']->ID])){
            $meta[]=array(
                    'course_id'=>$course->ID,
                    'module_id'=>$lesson['post']->ID,
                    'module_name'=>$lesson['post']->post_name,
                    'module_title'=>$lesson['post']->post_title,
                );
            }
            }
        }
    return (array) $meta;
}
public function get_course_sectioned_modules(){
    global $wpdb;
    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
    $meta= array();
    foreach($courses as $course){
        $lessons=learndash_get_course_lessons_list($course->ID);
        foreach($lessons as $lesson){
        $sections= learndash_30_get_course_sections($course->ID );
            if(isset($sections[$lesson['post']->ID])){
            $meta[]=array(
                    'course_id'=>$course->ID,
                    'module_id'=>$lesson['post']->ID,
                    'module_name'=>$lesson['post']->post_name,
                    'module_title'=>$lesson['post']->post_title,
                );
            }
            }
        }
    return (array) $meta;
}
public function get_course_modules_lessons(){
    global $wpdb;
    global $wp;
    $user_id = get_current_user_id();
    $current_page= get_the_ID();
    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
    $meta= array();
    foreach($courses as $course){
        $lessons=learndash_get_course_lessons_list($course->ID);
        echo $setting_status;
        foreach($lessons as $lesson){
            $topics= learndash_course_get_topics( $course->ID,$lesson['post']->ID);
            foreach($topics as $topic){
            $topic_orginal_premalink = learndash_get_step_permalink(  $topic->ID, $course->ID );
                $status = ( learndash_is_item_complete($topic->ID,$user_id, $course->ID ) ? 'complete' : 'incomplete' );
                $meta[]= array(
                'module_id'=>$lesson['post']->ID,
                'course_id'=>$course->ID,
                'module_title'=> $lesson['post']->post_title,
                'course_title'=> $course->post_title,
                'topic_id'=>$topic->ID,
                'topic_status'=>$topic->post_status,
                'topic_title'=>$topic->post_title,
                'topic_user_status'=>$status,
                'topic_premalink' =>$topic_orginal_premalink,
                'current_page'=>$current_page
                );
            }
        }
    }
    return (array) $meta;
}
public function get_name(){
    return 'learndash-course-navigation-widget';
}
public function get_title(){
    return esc_html__('Navigation','elementor-custom-widgets-for-learndash');
}
public function get_script_depends(){
    return[
        'elcw-script'
    ];
}
public function get_icon(){
    return 'eicon-post-list';
}
public function get_categories(){
    return[
        'learndash-custom-widgets-for-elementor'
    ];
}
public function _register_controls() {
    //Content Setings
    $this->start_controls_section(
        'content_setting',
        [
            'label'=>__('Settings','elementor-custom-widgets-for-learndash'),
            'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
        ]
    );

    $this->add_control(
        'hidden_course_modules_lessons',
        [
            'label' => __( 'View', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::HIDDEN,
            'default' => $this->get_course_modules_lessons(),
        ]
    );
    $this->add_control(
        'hidden_course_all_modules',
        [
            'label' => __( 'View', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::HIDDEN,
            'default' => $this->get_course_all_modules(),
        ]
    );
    $this->add_control(
        'hidden_course_sectioned_modules',
        [
            'label' => __( 'View', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::HIDDEN,
            'default' => $this->get_course_sectioned_modules(),
        ]
    );
    $this->add_control(
        'hidden_course_none_sectioned_modules',
        [
            'label' => __( 'View', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::HIDDEN,
            'default' => $this->get_course_none_sectioned_modules(),
        ]
    );
    $this->add_control(
        'course_id',
        [
            'label' => __( 'Course', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SELECT2,
            'multiple' => false,
            'options' =>  $this->get_courses()
        ]
    );
    $this->add_control(
        'hierarchie',
        [
            'label' => esc_html__( 'Hierarchie', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'dynamic' => [
                'active' => true,
            ],
            'options' => [
                'default' => esc_html__( 'Course', 'elementor-custom-widgets-for-learndash' ),
            'section' => esc_html__( 'Section', 'elementor-custom-widgets-for-learndash' ),
            'lesson' => esc_html__( 'Lesson', 'elementor-custom-widgets-for-learndash' ),
            ],
            'default' => 'default',
        ]
    );
    $this->end_controls_section();
    $this->start_controls_section(
        'display_setting',
        [
            'label'=>__('Display Settings','elementor-custom-widgets-for-learndash'),
            'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
        ]
    );
    $this->add_control(
        'show_breadcrumbs',
        [
            'label' => __( 'Show Breadcrumbs', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    $this->add_control(
        'custom_label_breadcrumbs',
        [
            'label' => __( 'Custom Breadcrumbs Label', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Yes', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'No', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'no',
        ]
    );
    $this->add_control(
        'custom_label_breadcrumbs_text',
        [
            'label' =>  esc_html__( 'Text', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'label_block' => true,
            'dynamic' => [
                'active' => true,
            ],
            'default' => __( 'Home', 'elementor-custom-widgets-for-learndash' ),
            'placeholder' => __( 'Add Text Here', 'elementor-custom-widgets-for-learndash' ),
        ]
    );
    $this->add_control(
        'link_breadcrumbs',
        [
            'label' => esc_html__( 'Link', 'elementor' ),
            'type' => Controls_Manager::URL,
            'dynamic' => [
                'active' => true,
            ],
            'default' => [
                'url' => '',
            ],
            'placeholder' => __( 'http://dein-link.de/', 'elementor-custom-widgets-for-learndash' ),

        ]
    );

    $this->end_controls_section();

    //style tab
    $this->start_controls_section(
        'breadcrumbs_tab',
        [
            'label' => __( 'Breadcrumbs', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    );

    $this->add_control(
        'breadcrumbs_text_padding',
        [
            'label' => __( 'Breadcrumb Padding', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 50,
                    'step' => 1,
                ],
            ],
            'default' => [
                'size' => 10,
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-breadcrumbs-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
            ],

        ]
    );
    $this->add_control(
        'heading_1',
        [
            'label' => __( 'Text', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'before',
        ]
    );
    $this->add_control(
        'breadcrumbs_text_color',
        [
            'label' => __( 'Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-breadcrumbs-style a' => 'color: {{VALUE}}',
            ],
            'default' => '#a9a9a9',
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'breadcrumbs_text_typography',
            'label' => __( 'Typography', 'plugin-domain' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-breadcrumbs-style a',
        ]
    );
    $this->add_control(
        'heading_2',
        [
            'label' => __( 'Active Text', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'before',
        ]
    );
    $this->add_control(
        'breadcrumbs_active_text_color',
        [
            'label' => __( 'Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-breadcrumbs-style.active a' => 'color: {{VALUE}}',
            ],
            'default' => '#82cbc5',
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'breadcrumbs_active_text_typography',
            'label' => __( 'Typography', 'plugin-domain' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-breadcrumbs-style.active a',
            'separator' => 'after'
            ]
    );
    $this->end_controls_section();
    $this->start_controls_section(
        'progress_tab',
        [
            'label' => __( 'Progress', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    );

    $this->add_control(
        'progress_background_stroke_color',
        [
            'label' => __( 'Background Stroke Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-navigation-widget .circle-progress .ProgressBar-background' => 'stroke: {{VALUE}}',
            ],
            'default' => '#dddddd',
        ]
    );
    $this->add_control(
        'progress_value_stroke_color',
        [
            'label' => __( 'Value Stroke Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-navigation-widget .circle-progress .ProgressBar-circle' => 'stroke: {{VALUE}}',
            ],
            'default' => '#82cbc5',
        ]
    );

    $this->end_controls_section();
    $this->start_controls_section(
        'navigation_tab',
        [
            'label' => __( 'Navigation', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    );
    $this->add_control(
        'heading_3',
        [
            'label' => __( 'Lessons', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,

        ]
    );
    $this->add_control(
        'lessons_text_color',
        [
            'label' => __( 'Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-lessons-style a' => 'color: {{VALUE}}',
            ],
            'default' => 'black',
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'lessons_text_typography',
            'label' => __( 'Typography', 'plugin-domain' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-lessons-style a',
        ]
    );

    $this->add_control(
        'heading_4',
        [
            'label' => __( 'Topics', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'before',

        ]
    );
    $this->add_control(
        'topics_text_color',
        [
            'label' => __( 'Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-topics-style-text' => 'color: {{VALUE}}',
            ],
            'default' => '#5F5F5F',
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'topics_text_typography',
            'label' => __( 'Typography', 'plugin-domain' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-topics-style-text ',
        ]
    );
    $this->add_control(
        'topics_text_background',
        [
            'label' => __( 'Background Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
           'selectors' => [
                '{{WRAPPER}} a.widget-course-navigation-topics-style' => 'background-color: {{VALUE}}',
            ],
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'topics_text_border',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-topics-style.border',
        ]
    );
    $this->add_responsive_control(
        'topics_text_radius',
        [
            'label' => __( 'Border Radius', 'elementor-custom-widgets-for-learndash' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => [ 'px', '%', 'em' ],
            'default' => [
                'top' => 0,
                'right' => 0,
                'bottom' => 0,
                'left' => 0,
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-topics-style' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Box_Shadow::get_type(),
        [
            'name' => 'topics_text_shadow',
            'label' => __( 'Box Shadow', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-topics-style.border',
        ]
    );

    $this->add_control(
        'topics_text_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 50,
                    'step' => 1,
                ],
            ],
            'default' => [
                'size' => 5,
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-topics-style' => 'padding: {{SIZE}}{{UNIT}};',
            ],
        ]
    );

    $this->add_control(
        'heading_5',
        [
            'label' => __( 'Active Topics', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'before',

        ]
    );
    $this->add_control(
        'active_topics_text_color',
        [
            'label' => __( 'Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .active .widget-course-navigation-topics-style-text , {{WRAPPER}} a:hover .widget-course-navigation-topics-style-text' => 'color: {{VALUE}}',
            ],
            'default' => '#73c0b1',
        ]
    );
    $this->add_control(
        'active_topics_text_background',
        [
            'label' => __( 'Background Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} a.widget-course-navigation-topics-style.active , {{WRAPPER}} a.widget-course-navigation-topics-style:hover ' => 'background-color: {{VALUE}}',
            ],
            'default' => '#e5e5e5',
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'active_topics_text_border',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} a.widget-course-navigation-topics-style.active , {{WRAPPER}} a.widget-course-navigation-topics-style:hover ',
        ]
    );
    $this->add_responsive_control(
        'active_topics_text_radius',
        [
            'label' => __( 'Border Radius', 'elementor-custom-widgets-for-learndash' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => [ 'px', '%', 'em' ],
            'default' => [
                'top' => 0,
                'right' => 0,
                'bottom' => 0,
                'left' => 0,
            ],
            'selectors' => [
                '{{WRAPPER}} a.widget-course-navigation-topics-style.active , {{WRAPPER}} a.widget-course-navigation-topics-style:hover ' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Box_Shadow::get_type(),
        [
            'name' => 'active_topics_text_shadow',
            'label' => __( 'Box Shadow', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} a.widget-course-navigation-topics-style.active , {{WRAPPER}} a.widget-course-navigation-topics-style:hover ',
        ]
    );

    $this->add_control(
        'active_topics_text_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 50,
                    'step' => 1,
                ],
            ],
            'default' => [
                'size' => 5,
            ],
            'selectors' => [
                '{{WRAPPER}} a.widget-course-navigation-topics-style.active , {{WRAPPER}} a.widget-course-navigation-topics-style:hover ' => 'padding: {{SIZE}}{{UNIT}};',
            ],
        ]
    );
    $this->add_control(
        'lesson_topic_padding',
        [
            'label' => __( 'Lesson & Topic Padding', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 50,
                    'step' => 1,
                ],
            ],
            'default' => [
                'size' => 5,
            ],
            'separator' => 'before',
                'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-topics-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
            ],
        ]
    );
    $this->end_controls_section();
    $this->start_controls_section(
        'Icon_tab',
        [
            'label' => __( 'Icon', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    );
    $this->add_control(
        'heading_6',
        [
            'label' => __( 'Active Topics', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'Progress Icon',

        ]
    );
    $this->add_responsive_control(
        'icon_size',
        [
            'label' => __( 'Size', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px'  ],
            'description' => 'Default: 20px',
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 60,
                    'step' => 1,
                ],
            ],
            'default' => [
                'unit' => 'px',
                'size' => 20,
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style' => 'font-size: {{SIZE}}{{UNIT}};',
            ],
        ]
    );

    $this->add_responsive_control(
        'icon_rotation',
        [
            'label' => __( 'Rotation', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'deg' ],
            'default' => [
                'size' => 0,
                'unit' => 'deg',
            ],
            'tablet_default' => [
                'unit' => 'deg',
            ],
            'mobile_default' => [
                'unit' => 'deg',
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style i, {{WRAPPER}} .widget-course-navigation-icon-style svg' => 'transform: rotate({{SIZE}}{{UNIT}});',
            ],
        ]
    );
    $this->add_control(
        'selected_icon_completed',
        [
            'label' => esc_html__( 'Status "Completed"', 'elementor' ),
            'type' => Controls_Manager::ICONS,
            'fa4compatibility' => 'icon',
            'default' => [
                'value' => 'fas fa-check-circle',
                'library' => 'fa-solid',
            ],
        ]
    );
    $this->add_control(
        'selected_icon_completed_view',
        [
            'label' => esc_html__( 'Display ', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'default' => esc_html__( 'Default', 'elementor' ),
                'stacked' => esc_html__( 'Stacked', 'elementor' ),
                'framed' => esc_html__( 'Framed', 'elementor' ),
            ],
            'default' => 'default',
            'prefix_class' => 'completed-view-',
        ]
    );


    $this->add_control(
        'selected_icon_completed_shape',
        [
            'label' => esc_html__( 'Shape', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'circle' => esc_html__( 'Circle', 'elementor' ),
                'square' => esc_html__( 'Square', 'elementor' ),
            ],
            'default' => 'circle',
            'condition' => [
                'selected_icon_completed_view!' => 'default',
            ],
            'prefix_class' => 'completed-shape-',
        ]
    );

    $this->add_control(
        'selected_icon_completed_icon_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.completed i' => 'color: {{VALUE}}',
                '{{WRAPPER}} .widget-course-navigation-icon-style.completed svg' => 'fill: {{VALUE}}',
            ],
            'default' => '#5F5F5F',
        ]
    );
    $this->add_control(
        'selected_icon_completed_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.completed i, {{WRAPPER}} .widget-course-navigation-icon-style.completed svg' => 'background-color: {{VALUE}}',
            ],
            'condition' => [
                'selected_icon_completed_view!' => 'default',
            ],
        ]
    );

    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'completed_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-icon-style.completed i, {{WRAPPER}} .widget-course-navigation-icon-style.completed svg',
            'condition' => [
                'selected_icon_completed_view' => 'framed',
            ],
        ]
    );
        $this->add_responsive_control(
        'selected_icon_completed_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS,

            'size_units' => [ 'px'  ],
            'default' => [
                'top' => 3,
                'right' => 3,
                'bottom' => 3,
                'left' => 3,
            ],
            'condition' => [
                'selected_icon_completed_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.completed i, {{WRAPPER}} .widget-course-navigation-icon-style.completed svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );

    $this->add_control(
        'selected_icon_incomplete',
        [
            'label' => esc_html__( 'Status "Incomplete"', 'elementor' ),
            'type' => Controls_Manager::ICONS,
            'fa4compatibility' => 'icon',
            'default' => [
                'value' => 'fas fa-play-circle',
                'library' => 'fa-solid',
            ],
            'separator' => 'before',
        ]
    );

    $this->add_control(
        'selected_icon_incomplete_view',
        [
            'label' => esc_html__( 'Icon ', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'default' => esc_html__( 'Default', 'elementor' ),
                'stacked' => esc_html__( 'Stacked', 'elementor' ),
                'framed' => esc_html__( 'Framed', 'elementor' ),
            ],
            'default' => 'default',
            'prefix_class' => 'incomplete-view-',
        ]
    );

    $this->add_control(
        'selected_icon_incomplete_shape',
        [
            'label' => esc_html__( 'Shape', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'circle' => esc_html__( 'Circle', 'elementor' ),
                'square' => esc_html__( 'Square', 'elementor' ),
            ],
            'default' => 'circle',
            'condition' => [
                'selected_icon_incomplete_view!' => 'default',
            ],
            'prefix_class' => 'incomplete-shape-',
        ]
    );
    $this->add_control(
        'selected_icon_incomplete_icon_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.incomplete i' => 'color: {{VALUE}}',
                '{{WRAPPER}} .widget-course-navigation-icon-style.incomplete svg' => 'fill: {{VALUE}}',
            ],

            'default' => '#5F5F5F',
        ]
    );
    $this->add_control(
        'selected_icon_incomplete_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.incomplete i, {{WRAPPER}} .widget-course-navigation-icon-style.incomplete svg' => 'background-color: {{VALUE}}',
            ],
            'condition' => [
                'selected_icon_incomplete_view!' => 'default',
            ],
        ]
    );

    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'incomplete_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-icon-style.incomplete i, {{WRAPPER}} .widget-course-navigation-icon-style.incomplete svg',
            'condition' => [
                'selected_icon_incomplete_view' => 'framed',
            ],
        ]
    );
    $this->add_responsive_control(
        'selected_icon_incomplete_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => [ 'px'  ],
            'default' => [
                'top' => 3,
                'right' => 3,
                'bottom' => 3,
                'left' => 3,
            ],
            'condition' => [
                'selected_icon_incomplete_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.incomplete i, {{WRAPPER}} .widget-course-navigation-icon-style.incomplete svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'incomplete_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-icon-style.incomplete i, {{WRAPPER}} .widget-course-navigation-icon-style.incomplete svg',
            'condition' => [
                'selected_icon_incomplete_view' => 'framed',
            ],
        ]
    );
    $this->add_control(
        'heading_7',
        [
            'label' => __( 'Hover and Active Icon Style', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'before',
        ]
    );

    $this->add_control(
        'selected_icon_active_icon_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.active i' => 'color: {{VALUE}}',
                '{{WRAPPER}} .widget-course-navigation-icon-style.active svg' => 'fill: {{VALUE}}',
                '{{WRAPPER}} a:hover .widget-course-navigation-icon-style i' => 'color: {{VALUE}}',
                '{{WRAPPER}} a:hover .widget-course-navigation-icon-style svg' => 'fill: {{VALUE}}',
            ],
            'default' => '#82cbc5',
        ]
    );
    $this->add_control(
        'selected_icon_active_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.active i, {{WRAPPER}} .widget-course-navigation-icon-style.active svg' => 'background-color: {{VALUE}}',
                '{{WRAPPER}} a:hover .widget-course-navigation-icon-style.completed i, {{WRAPPER}} a:hover .widget-course-navigation-icon-style.completed svg' => 'background-color: {{VALUE}}',
                '{{WRAPPER}} a:hover .widget-course-navigation-icon-style.incomplete i, {{WRAPPER}} a:hover .widget-course-navigation-icon-style.incomplete svg' => 'background-color: {{VALUE}}',
            ],

        ]
    );
    $this->add_control(
        'active_border_width',
        [
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.active i, {{WRAPPER}} .widget-course-navigation-icon-style.active svg' => 'border-color: {{VALUE}}',
                '{{WRAPPER}} a:hover .widget-course-navigation-icon-style.completed i, {{WRAPPER}} a:hover .widget-course-navigation-icon-style.completed svg' => 'border-color: {{VALUE}}',
                '{{WRAPPER}} a:hover .widget-course-navigation-icon-style.incomplete i, {{WRAPPER}} a:hover .widget-course-navigation-icon-style.incomplete svg' => 'border-color: {{VALUE}}',
            ],
        ]
    );

    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'active_icon_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-icon-style.active i, {{WRAPPER}} .widget-course-navigation-icon-style.active svg',
            'condition' => [
                'selected_icon_active_view' => 'framed',
            ],
        ]
    );
    $this->add_responsive_control(
        'selected_icon_active_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS,

            'size_units' => [ 'px'  ],
            'default' => [
                'top' => 3,
                'right' => 3,
                'bottom' => 3,
                'left' => 3,
            ],
            'condition' => [
                'selected_icon_active_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-style.active i, {{WRAPPER}} .widget-course-navigation-icon-style.active svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );

    $this->add_control(
        'heading_8',
        [
            'label' => __( 'Collapse Menu Icon', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'before',

        ]
    );
    $this->add_responsive_control(
        'icon_collapse_size',
        [
            'label' => __( 'Size', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px'  ],
            'description' => 'Default: 20px',
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 60,
                    'step' => 1,
                ],
            ],
            'default' => [
                'unit' => 'px',
                'size' => 20,
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-collapse-style' => 'font-size: {{SIZE}}{{UNIT}};',
            ],
        ]
    );

    $this->add_control(
        'selected_icon_collapse',
        [
            'label' => esc_html__( 'Status "collapse"', 'elementor' ),
            'type' => Controls_Manager::ICONS,
            'fa4compatibility' => 'icon',
            'default' => [
                'value' => 'fas fa-caret-right',
                'library' => 'fa-solid',
            ],
        ]
    );
    $this->add_control(
        'selected_icon_collapse_view',
        [
            'label' => esc_html__( 'Display ', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'default' => esc_html__( 'Default', 'elementor' ),
                'stacked' => esc_html__( 'Stacked', 'elementor' ),
                'framed' => esc_html__( 'Framed', 'elementor' ),
            ],
            'default' => 'default',
            'prefix_class' => 'collapse-view-',
        ]
    );


    $this->add_control(
        'selected_icon_collapse_shape',
        [
            'label' => esc_html__( 'Shape', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'circle' => esc_html__( 'Circle', 'elementor' ),
                'square' => esc_html__( 'Square', 'elementor' ),
            ],
            'default' => 'circle',
            'condition' => [
                'selected_icon_collapse_view!' => 'default',
            ],
            'prefix_class' => 'collapse-shape-',
        ]
    );

    $this->add_control(
        'selected_icon_collapse_icon_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse i' => 'color: {{VALUE}}',
                '{{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse svg' => 'fill: {{VALUE}}',
            ],
            'default' => 'black',
        ]
    );

    $this->add_control(
        'selected_icon_collapse_active_color',
        [
            'label' => __( 'Active Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse.active i, {{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse:hover i, {{WRAPPER}} .ac-title.open-collapse .widget-course-navigation-icon-collapse-style.collapse i ' => 'color: {{VALUE}}',
                '{{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse.active svg, {{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse:hover svg, {{WRAPPER}} .ac-title.open-collapse .widget-course-navigation-icon-collapse-style.collapse svg' => 'fill: {{VALUE}}',
            ],
            'default'=>'#82cbc5'
        ]
    );
    $this->add_control(
        'selected_icon_collapse_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse i, {{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse svg' => 'background-color: {{VALUE}}',
            ],
            'condition' => [
                'selected_icon_collapse_view!' => 'default',
            ],
        ]
    );

    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'collapse_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse i, {{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse svg',
            'condition' => [
                'selected_icon_collapse_view' => 'framed',
            ],
        ]
    );
    $this->add_responsive_control(
        'selected_icon_collapse_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS,

            'size_units' => [ 'px'  ],
            'default' => [
                'top' => 3,
                'right' => 3,
                'bottom' => 3,
                'left' => 3,
            ],
            'condition' => [
                'selected_icon_collapse_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse i, {{WRAPPER}} .widget-course-navigation-icon-collapse-style.collapse svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
            'separator' => 'after',
        ]
    );
    $this->add_control(
        'heading_9',
        [
            'label' => __( 'Course Completed Icon', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'before',

        ]
    );
    $this->add_responsive_control(
        'icon_course_completed_size',
        [
            'label' => __( 'Size', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px'  ],
            'description' => 'Default: 20px',
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 60,
                    'step' => 1,
                ],
            ],
            'default' => [
                'unit' => 'px',
                'size' => 23,
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-course-completed-style' => 'font-size: {{SIZE}}{{UNIT}};',
            ],
        ]
    );

    $this->add_control(
        'selected_icon_course_completed',
        [
            'label' => esc_html__( 'Status "collapse"', 'elementor' ),
            'type' => Controls_Manager::ICONS,
            'fa4compatibility' => 'icon',
            'default' => [
                'value' => 'far fa-check-circle',
                'library' => 'fa-regular',
            ],
        ]
    );
    $this->add_control(
        'selected_icon_course_completed_view',
        [
            'label' => esc_html__( 'Display ', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'default' => esc_html__( 'Default', 'elementor' ),
                'stacked' => esc_html__( 'Stacked', 'elementor' ),
                'framed' => esc_html__( 'Framed', 'elementor' ),
            ],
            'default' => 'default',
            'prefix_class' => 'collapse-view-',
        ]
    );


    $this->add_control(
        'selected_icon_course_completed_shape',
        [
            'label' => esc_html__( 'Shape', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'circle' => esc_html__( 'Circle', 'elementor' ),
                'square' => esc_html__( 'Square', 'elementor' ),
            ],
            'default' => 'circle',
            'condition' => [
                'selected_icon_course_completed_view!' => 'default',
            ],
            'prefix_class' => 'course-completed-',
        ]
    );

    $this->add_control(
        'selected_icon_course_completed_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-course-completed-style i' => 'color: {{VALUE}}',
                '{{WRAPPER}} .widget-course-navigation-icon-course-completed-style svg' => 'fill: {{VALUE}}',
            ],
            'default' => '#82CBC5',
        ]
    );

    $this->add_control(
        'selected_icon_course_completed_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-course-completed-style i, {{WRAPPER}} .widget-course-navigation-icon-course-completed-style svg' => 'background-color: {{VALUE}}',
            ],
            'condition' => [
                'selected_icon_course_completed_view!' => 'default',
            ],
        ]
    );

    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'selected_icon_course_completed_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .widget-course-navigation-icon-course-completed-style i, {{WRAPPER}} .widget-course-navigation-icon-course-completed-style svg',
            'condition' => [
                'selected_icon_course_completed_view' => 'framed',
            ],
        ]
    );
    $this->add_responsive_control(
        'selected_icon_course_completed_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS,

            'size_units' => [ 'px'  ],
            'default' => [
                'top' => 3,
                'right' => 3,
                'bottom' => 3,
                'left' => 3,
            ],
            'condition' => [
                'selected_icon_course_completed_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .widget-course-navigation-icon-course-completed-style i, {{WRAPPER}} .widget-course-navigation-icon-course-completed-style svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
            'separator' => 'after',
        ]
    );


    $this->end_controls_section();



}

protected function render(){
    $this->get_breadcrumbs();
    $breadcrumbs=$this->get_breadcrumbs();
    $settings = $this->get_settings_for_display();
    $widget_view= $settings['hierarchie'];
    $course_id= $settings['course_id'];
    $target = $settings['link_breadcrumbs']['is_external'] ? ' target="_blank"' : '';
    $nofollow = $settings['link_breadcrumbs']['nofollow'] ? ' rel="nofollow"' : '';
    if($widget_view == "default"){
        $course_modules=$settings['hidden_course_all_modules'];
    }
    elseif($widget_view =="section"){
        $course_modules=$settings['hidden_course_sectioned_modules'];

    }
    else{
        $course_modules=$settings['hidden_course_none_sectioned_modules'];

    }
    $course_modules_lessons= $settings['hidden_course_modules_lessons'];


        include  ELCW_PLUGIN_PATH_template.'/learndash-course-navigation-template.php';

}
protected function _content_template() {
?>
<#
var iconCollapse = elementor.helpers.renderIcon( view, settings.selected_icon_collapse, { 'aria-hidden': true }, 'i' , 'object' );
var iconCompleted = elementor.helpers.renderIcon( view, settings.selected_icon_completed, { 'aria-hidden': true }, 'i' , 'object' );
var iconIncomplete = elementor.helpers.renderIcon( view, settings.selected_icon_incomplete, { 'aria-hidden': true }, 'i' , 'object' );
var iconActive = elementor.helpers.renderIcon( view, settings.selected_icon_active, { 'aria-hidden': true }, 'i' , 'object' );
var iconCourseCompleted = elementor.helpers.renderIcon( view, settings.selected_icon_course_completed, { 'aria-hidden': true }, 'i' , 'object' );
var widget_view=  settings.hierarchie;
var course_id=  settings.course_id;
var target = settings.link_breadcrumbs.is_external ? ' target="_blank"' : '';
var nofollow = settings.link_breadcrumbs.nofollow ? ' rel="nofollow"' : '';
var course_modules_lessons= settings.hidden_course_modules_lessons;
        if(widget_view == "default"){
        var course_modules=settings.hidden_course_all_modules ;
        }
        else if(widget_view =="section"){
        var course_modules=settings.hidden_course_sectioned_modules ;
        }
        else{
        var course_modules=settings.hidden_course_none_sectioned_modules ;
        }
        #>

        <?php
            include  ELCW_PLUGIN_PATH_template.'/learndash-course-navigation-frontend-template.php';



}
}
Plugin::instance()->widgets_manager->register_widget_type( new LEARNDASH_COURSE_NAVIGATION_WIDGET());