<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
if ( ! defined( 'ABSPATH' ) ) {
	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	include_once $parse_uri[0] . '/wp-load.php';
}

define( 'WP_USE_THEMES', true );

global $wpdb, $bp;
use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Core\Kits\Documents\Tabs\Global_Typography;
define( 'ELCW_PLUGIN_PATH_template', dirname( __FILE__ ) . '/templates' );

class LEARNDASH_COURSE_GRID_WIDGET extends Widget_Base{
public function get_carousel_rand_attr(){
    $rand= rand();
    return $rand;
}
public function get_courses(){
    global $wpdb; 
    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
    $items=array();
    foreach($courses as $course){     
        $items[$course->ID ]= $course->post_title;
    }   
    return (array) $items;	 

}
public function get_latest_user_activity(){
    global $wpdb; 
    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
    $meta= array();
    foreach($courses as $course){ 
        $user_id = get_current_user_id();
    $last_activity=$wpdb->get_results("SELECT * FROM `".$wpdb->prefix."learndash_user_activity` WHERE `user_id` = $user_id AND `course_id` = $course->ID AND `activity_status` = 1 AND `activity_type` = 'lesson' ORDER BY `activity_id` DESC LIMIT 1");
    foreach($last_activity as $activity){  
        $meta[$course->ID] = $activity->post_id;
    }
 }
 
 return (array) $meta;
}
public function get_course_all_modules(){  
    global $wpdb; 
    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
    $meta= array();
    foreach($courses as $course){ 
        $user_id = get_current_user_id();
        $lessons=learndash_get_course_lessons_list($course->ID);  
    foreach($lessons as $lesson){
            $topics= learndash_course_get_topics( $course->ID,$lesson['post']->ID);   
            $first_topic= array_shift(array_slice($topics, 0, 1));  
            $topicPremalink= learndash_get_step_permalink(  $first_topic->ID, $course->ID );  
            $excerpt=  get_post_meta( $lesson['post']->ID, '_learndash_course_grid_short_description', true );
            $lesson_thumb= get_the_post_thumbnail_url($lesson['post']->ID, 'full' ); 
            $lesson_progress = learndash_lesson_progress( $lesson['post']->ID, $course->ID );
            if($lesson_progress['percentage'] == Null && $lesson['status'] == "completed"){
                $steps= 100;
            } 
            else if($lesson_progress['percentage'] == Null && $lesson['status'] == "notcompleted"){
                $steps = 0;
            }
            else{
                $steps =$lesson_progress['percentage'];
            }  
            if($topicPremalink != NULL){
                $cardPremalink= $topicPremalink;
            }
            else{
                $cardPremalink= $lesson['permalink'];
            }
            $meta[]=array(
                    'course_id'=>$course->ID,
                    'course_name'=>$course->post_title,
                    'module_id'=>$lesson['post']->ID,
                    'module_name'=>$lesson['post']->post_name,
                    'module_title'=>$lesson['post']->post_title,
                    'module_excerpt'=>$excerpt,
                    'module_premalink'=>$cardPremalink,
                    'status'=>$lesson['status'],
                    'steps'=>$steps,
                    'image_url'=>$lesson_thumb,
                    'post_status'=>$lesson['post']->post_status
                ); 
            }
        }             
    return (array) $meta;
}
public function get_name(){
    return 'learndash-course-grid-widget';
}
public function get_title(){
    return esc_html__(' Course Grid','elementor-custom-widgets-for-learndash');
}
public function get_script_depends(){
    return[
        'elcw-script'
    ];
}
public function get_icon(){
    return 'eicon-bullet-list';
}
public function get_categories(){
    return[
        'learndash-custom-widgets-for-elementor'
    ];
}
public function _register_controls() {
    //Content Setings 
    $this->start_controls_section(
        'content_setting',
        [
            'label'=>__('Setting','elementor-custom-widgets-for-learndash'),
            'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
        ]
    );
    $this->add_control(
        'course_id',
        [ 
            'label' => __( 'Course', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SELECT2,
            'multiple' => false,
            'options' =>  $this->get_courses()
        ]
    );
    $this->add_control(
        'hidden_course_all_modules',
        [
            'label' => __( 'View', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::HIDDEN,
            'default' => $this->get_course_all_modules(),
        ]
    );
    $this->add_control(
        'hidden_rand_id',
        [
            'label' => __( 'View', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::HIDDEN,
            'default' => $this->get_carousel_rand_attr(),
        ]
    );
    $this->add_control(
        'hidden_user_activity',
        [
            'label' => __( 'View', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::HIDDEN,
            'default' => $this->get_latest_user_activity(),
        ]
    );
    $this->end_controls_section();
        //Content Setings 
    $this->start_controls_section(
        'content_display_setting',
        [
            'label'=>__('Display Setting','elementor-custom-widgets-for-learndash'),
            'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
        ]
    );
    $this->add_control(
        'heading',
        [
            'label' => __( 'Layout', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,   
        ]
    ); 
    $this->add_control(
        'show_grid',
        [
            'label' => __( 'Show Grid', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    
    $this->add_Control(
        'items_to_show_desktop',
        [
            'label' => esc_html__( 'Desktop Slides Number', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [ 
                 '1'=>'1',
                 '2'=>'2',
                 '3'=>'3',
                 '4'=>'4'
             ],
             'default' =>'3', 
        ]
    );
    $this->add_Control(
        'items_to_show_tablet',
        [
            'label' => esc_html__( 'Tablet Slides Number', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [ 
                 '1'=>'1',
                 '2'=>'2',
                 '3'=>'3',
                 '4'=>'4'
             ],
             'default' =>'2', 
        ]
    );
    $this->add_Control(
        'items_to_show_mobile',
        [
            'label' => esc_html__( 'Mobile Slides Number', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [ 
                 '1'=>'1',
                 '2'=>'2',
                 '3'=>'3',
                 '4'=>'4'
             ],
             'default' =>'1', 
        ]
    );
    $this->add_control(
        'show_carousel',
        [
            'label' => __( 'Show Carousel', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    $this->add_control(
        'show_next_module',
        [
            'label' => __( 'Order Show Next Module', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes', 
            'condition' => [
				'show_carousel' => 'yes',
			],
			'of_type' => 'no',
            
        ]
    );
    $this->add_control(
        'heading_2',
        [
            'label' => __( 'Content', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,
            'separator' => 'before',    
        ]
    ); 
    $this->add_control(
        'show_media',
        [
            'label' => __( 'Media', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    $this->add_control(
        'show_post_title',
        [
            'label' => __( 'Post Title', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    $this->add_control(
        'show_excerpt',
        [
            'label' => __( 'Excerpt', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    $this->add_control(
        'show_icon',
        [
            'label' => __( 'Icon', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    $this->add_control(
        'show_progress_bar',
        [
            'label' => __( 'Progress Bar', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    $this->add_control(
        'show_start_btn',
        [
            'label' => __( 'Start Button', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );

    $this->end_controls_section();
    $this->start_controls_section(
        'box_tab',
        [
            'label' => __( 'Box', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    ); 
    
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'box_border',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-box-style',
        ]
    );
    $this->add_responsive_control(
        'border_box_radius',
        [
            'label' => __( 'Border Radius', 'elementor-custom-widgets-for-learndash' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => [ 'px', '%', 'em' ],
            'default' => [
                'top' => 10,
                'right' => 10,
                'bottom' => 10,
                'left' => 10,
            ],
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-box-style' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};', 
            ],
        ]
    );
    $this->add_group_control(
        \Elementor\Group_Control_Box_Shadow::get_type(),
        [
            'name' => 'box_shadow',
            'label' => __( 'Box Shadow', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-box-style',
        ]
    );
    $this->add_group_control(
        Group_Control_Background::get_type(),
        [
            'name' => 'box_background_type',
            'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash' ),
            'types' => [ 'classic', 'gradient' ], 
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-box-style .box-back',
            'fields_options' => [
                'background' => [
                    'default' => 'classic',
                ], 
            ],
        ]
    );
    $this->add_control(
        'box_height',
        [
            'label' => esc_html__( 'Height', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'dynamic' => [
                'active' => true,
            ],
            'options' => [	
                'default' => esc_html__( 'Srandard', 'elementor-custom-widgets-for-learndash' ), 
            'custom' => esc_html__( 'Min.height', 'elementor-custom-widgets-for-learndash' ),
            ],
            'default' => 'default',
        ]
    ); 
    $this->add_responsive_control(
        'box_custom_height',
        [
            'label' => __( 'Custom Height', 'elementor-custom-widgets-for-learndash' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px', '%' ],
            'description' => 'Default: 300px',
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 1000,
                    'step' => 1,
                ],
                '%' => [
                    'min' => 0,
                    'max' => 100,
                ],
                'vh' => [
                    'min' => 0,
                    'max' => 100,
                ],
            ],
            'default' => [
                'unit' => 'px',
                'size' => 300,
            ],
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-box-style' => 'height: {{SIZE}}{{UNIT}};',
            ],
            'condition' => [
                'box_height' => 'custom'
            ]
        ]
    );
    
    $this->end_controls_section();
    $this->start_controls_section(
        'media_tab',
        [
            'label' => __( 'Media', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    ); 
    $this->add_control(
        'heading_3',
        [
            'label' => __( 'Media Overlay', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,   
        ]
    ); 
        $this->start_controls_tabs(
            'media_style_section_tabs'
        );
            // Normal State
            $this->start_controls_tab(
                'media_overlay_background_normal_state',
                [
                    'label' => __( 'Normal', 'elementor-custom-widgets-for-learndash' ),
                ]
            );
                
                $this->add_group_control(
                    Group_Control_Background::get_type(),
                    [
                        'name' => 'media_overlay_background_normal',
                        'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash' ),
                        'types' => [ 'classic', 'gradient' ], 
                        'selector' => '{{WRAPPER}} .learndash-course-grid-widget-overlay-style',
                        'fields_options' => [
                            'background' => [
                                'default' => 'classic',
                            ], 
                        ],
                    ]
                );

            $this->end_controls_tab();

            // Hover State
            $this->start_controls_tab(
                'media_overlay_background_hover_state',
                [
                    'label' => __( 'Hover', 'elementor-custom-widgets-for-learndash' ),
                ]
            );
                
            $this->add_group_control(
                Group_Control_Background::get_type(),
                [
                    'name' => 'media_overlay_background_hover',
                    'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash' ),
                    'types' => [ 'classic', 'gradient' ], 
                    'selector' => '{{WRAPPER}} .learndash-course-grid-widget .item:hover .learndash-course-grid-widget-overlay-style',
                    'fields_options' => [
                        'background' => [
                            'default' => 'classic',
                        ], 
                    ],
                ]
            );
            $this->end_controls_tab();

    $this->end_controls_tabs();
    $this->add_control(
        'heading_4',
        [
            'label' => __( 'Icon', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,  
            'separator' => 'before', 
        ]
    ); 
    $this->add_responsive_control(
            'icon_size',
            [
                'label' => __( 'Size', 'plugin-domain' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px'  ],
                'description' => 'Default: 20px',
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 60,
                        'step' => 1,
                    ], 
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 23,
                ],
                'selectors' => [
                    '{{WRAPPER}} .learndash-course-grid-widget-icon-style' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        
        $this->add_responsive_control(
            'icon_rotation',
            [
                'label' => __( 'Rotation', 'plugin-domain' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'deg' ],
                'default' => [
                    'size' => 0,
                    'unit' => 'deg',
                ],
                'tablet_default' => [
                    'unit' => 'deg',
                ],
                'mobile_default' => [
                    'unit' => 'deg',
                ],
                'selectors' => [
                    '{{WRAPPER}} .learndash-course-grid-widget-icon-style i, {{WRAPPER}} .learndash-course-grid-widget-icon-style svg' => 'transform: rotate({{SIZE}}{{UNIT}});',
                ],
            ]
        ); 
                
    $this->add_control(
        'selected_icon_closed',
        [
            'label' => esc_html__( 'Status "closed"', 'elementor' ),
            'type' => Controls_Manager::ICONS,
            'fa4compatibility' => 'icon',
            'default' => [
                'value' => 'fas fa-lock',
                'library' => 'fa-solid',
            ],
        ]
    );
    $this->add_control(
        'selected_icon_closed_view',
        [
            'label' => esc_html__( 'Display ', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'default' => esc_html__( 'Default', 'elementor' ),
                'stacked' => esc_html__( 'Stacked', 'elementor' ),
                'framed' => esc_html__( 'Framed', 'elementor' ),
            ],
            'default' => 'framed',
            'prefix_class' => 'closed-view-',
        ]
    );
    

    $this->add_control(
        'selected_icon_closed_shape',
        [
            'label' => esc_html__( 'Shape', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'circle' => esc_html__( 'Circle', 'elementor' ),
                'square' => esc_html__( 'Square', 'elementor' ),
            ],
            'default' => 'circle',
            'condition' => [
                'selected_icon_closed_view!' => 'default',
            ],
            'prefix_class' => 'closed-shape-',
        ]
    );

    $this->add_control(
        'selected_icon_closed_icon_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.closed i' => 'color: {{VALUE}}',
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.closed svg' => 'fill: {{VALUE}}',
            ],
            'default' => '#fff',
        ]
    );
    $this->add_control(
        'selected_icon_closed_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.closed i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.closed svg' => 'background-color: {{VALUE}}',
            ], 
            'condition' => [
                'selected_icon_closed_view!' => 'default',
            ], 
        ]
    ); 
    
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'closed_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-icon-style.closed i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.closed svg',
            'condition' => [
                'selected_icon_closed_view' => 'framed',
            ],
        ]
    );
        $this->add_responsive_control(
        'selected_icon_closed_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS, 
            
            'size_units' => [ 'px'  ], 
            'default' => [
                'top' => 11,
                'right' => 11,
                'bottom' => 11,
                'left' => 11,
            ],
            'condition' => [
                'selected_icon_closed_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.closed i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.closed svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );
    
    $this->add_control(
        'selected_icon_completed',
        [
            'label' => esc_html__( 'Status "Completed"', 'elementor' ),
            'type' => Controls_Manager::ICONS,
            'fa4compatibility' => 'icon',
            'default' => [
                'value' => 'fas fa-check',
                'library' => 'fa-solid',
            ],
        ]
    );
    $this->add_control(
        'selected_icon_completed_view',
        [
            'label' => esc_html__( 'Display ', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'default' => esc_html__( 'Default', 'elementor' ),
                'stacked' => esc_html__( 'Stacked', 'elementor' ),
                'framed' => esc_html__( 'Framed', 'elementor' ),
            ],
            'default' => 'framed',
            'prefix_class' => 'completed-view-',
        ]
    );
    

    $this->add_control(
        'selected_icon_completed_shape',
        [
            'label' => esc_html__( 'Shape', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'circle' => esc_html__( 'Circle', 'elementor' ),
                'square' => esc_html__( 'Square', 'elementor' ),
            ],
            'default' => 'circle',
            'condition' => [
                'selected_icon_completed_view!' => 'default',
            ],
            'prefix_class' => 'completed-shape-',
        ]
    );

    $this->add_control(
        'selected_icon_completed_icon_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.completed i' => 'color: {{VALUE}}',
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.completed svg' => 'fill: {{VALUE}}',
            ],
            'default' => '#fff',
        ]
    );
    $this->add_control(
        'selected_icon_completed_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.completed i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.completed svg' => 'background-color: {{VALUE}}',
            ], 
            'condition' => [
                'selected_icon_completed_view!' => 'default',
            ], 
        ]
    ); 
    
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'completed_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-icon-style.completed i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.completed svg',
            'condition' => [
                'selected_icon_completed_view' => 'framed',
            ],
        ]
    );
        $this->add_responsive_control(
        'selected_icon_completed_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS, 
            
            'size_units' => [ 'px'  ], 
            'default' => [
                'top' => 11,
                'right' => 11,
                'bottom' => 11,
                'left' =>11,
            ],
            'condition' => [
                'selected_icon_completed_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.completed i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.completed svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );
    
    $this->add_control(
        'selected_icon_incomplete',
        [
            'label' => esc_html__( 'Status "Incomplete"', 'elementor' ),
            'type' => Controls_Manager::ICONS,
            'fa4compatibility' => 'icon',
            'default' => [
                'value' => 'fas fa-play',
                'library' => 'fa-solid',
            ],
            'separator' => 'before',
        ]
    );
    
    $this->add_control(
        'selected_icon_incomplete_view',
        [
            'label' => esc_html__( 'Icon ', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'default' => esc_html__( 'Default', 'elementor' ),
                'stacked' => esc_html__( 'Stacked', 'elementor' ),
                'framed' => esc_html__( 'Framed', 'elementor' ),
            ],
            'default' => 'framed',
            'prefix_class' => 'incomplete-view-',
        ]
    );  
    
    $this->add_control(
        'selected_icon_incomplete_shape',
        [
            'label' => esc_html__( 'Shape', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'circle' => esc_html__( 'Circle', 'elementor' ),
                'square' => esc_html__( 'Square', 'elementor' ),
            ],
            'default' => 'circle',
            'condition' => [
                'selected_icon_incomplete_view!' => 'default',
            ],
            'prefix_class' => 'incomplete-shape-',
        ]
    );
    $this->add_control(
        'selected_icon_incomplete_icon_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.incomplete i' => 'color: {{VALUE}}',
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.incomplete svg' => 'fill: {{VALUE}}',
            ],
            
            'default' => '#fff',
        ]
    );
    $this->add_control(
        'selected_icon_incomplete_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.incomplete i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.incomplete svg' => 'background-color: {{VALUE}}',
            ], 
            'condition' => [
                'selected_icon_incomplete_view!' => 'default',
            ],
        ]
    );
        
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'incomplete_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-icon-style.incomplete i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.incomplete svg',
            'condition' => [
                'selected_icon_incomplete_view' => 'framed',
            ],
        ]
    );
    $this->add_responsive_control(
        'selected_icon_incomplete_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS,  
            'size_units' => [ 'px'  ], 
            'default' => [
                'top' => 11,
                'right' => 11,
                'bottom' => 11,
                'left' => 14,
            ],
            'condition' => [
                'selected_icon_incomplete_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-icon-style.incomplete i, {{WRAPPER}} .learndash-course-grid-widget-icon-style.incomplete svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );            
    
    $this->end_controls_section();
    $this->start_controls_section(
        'content_tab',
        [
            'label' => __( 'Content', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    ); 
    $this->add_control(
        'heading_5',
        [
            'label' => __( 'Lesson Title', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,  
            'separator' => 'before', 
        ]
    );
    $this->add_control(
        'title_color',
        [
            'label' => __( 'Text Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-title-style' => 'color: {{VALUE}}',
            ], 
        ]
    );
    //Title Text Typography
    $this->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'title_text_typography',
            'label' => __( 'Text Typography', 'plugin-domain' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-title-style ',
        ]
    );
    $this->add_control(
        'title_text_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER, 
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 50,
                    'step' => 1,
                ], 
            ],
            'default' => [ 
                'size' => 10,
            ],
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-title-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
            ],
                
        ]
    );
         
    $this->add_control(
        'heading_7',
        [
            'label' => __( 'Excerpt', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING, 
            'separator' => 'before',   
        ]
    ); 
    // excerpt Color
    $this->add_control(
        'excerpt_color',
        [
            'label' => __( 'Text Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-excerpt-style' => 'color: {{VALUE}}',
            ], 
        ]
    );
    //excerpt Text Typography
    $this->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'excerpt_text_typography',
            'label' => __( 'Text Typography', 'plugin-domain' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-excerpt-style',
        ]
    );
    $this->add_control( 
        'excerpt_lenght',
        [
            'label' => __( 'Excerpt Length', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::NUMBER,
            'min' => 0,
            'max' => 500,	
            'step' => 1,
            'default' => 50,
            
        ]
    );
    $this->add_control(
        'excerpt_text_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER, 
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 100,
                    'step' => 1,
                ], 
            ],
            'default' => [ 
                'size' => 10,
            ],
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-excerpt-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
            ],  
                
        ]
    );
    
    $this->add_control(
        'heading_8',
        [
            'label' => __( 'link', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING, 
            'separator' => 'before',   
        ]
    ); 
    // link Color
    $this->add_control(
        'link_color',
        [
            'label' => __( 'Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-link-style' => 'color: {{VALUE}}',
            ],
            'default' => '#fff',
        ]
    );
    //link background
    $this->add_control(
        'link_back_color',
        [
            'label' => __( 'Background Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-link-style' => 'background-color: {{VALUE}}',
            ],
            'default' => '#93013c',
        ]
    );
   
    //link Text Typography
    $this->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'link_text_typography',
            'label' => __( 'Text Typography', 'plugin-domain' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-link-style ',
        ]
    );
    // link Padding
    $this->add_responsive_control(
        'link_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => [ 'px', '%', 'em' ],
            'default' => [
                'top' => 6,
                'right' => 10,
                'bottom' => 6,
                'left' => 10,
            ],
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-link-style' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]
    );
    //link border
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'link_border',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-link-style',
        ]
    ); 
     //link border color
     $this->add_control(
        'link_border_color',
        [
            'label' => __( 'Border Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-link-style' => 'border-color: {{VALUE}}',
            ],
            'default' => '#93013c',
        ]
    );
    
    
    $this->end_controls_section();
    $this->start_controls_section(
        'progress_tab',
        [
            'label' => __( 'Progress', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    ); 
    
    $this->add_control(
        'progress_show_value',
        [
            'label' => __( 'Show Progress Value', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SWITCHER,
            'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
            'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
            'return_value' => 'yes',
            'default' => 'yes',
        ]
    );
    $this->add_control(
        'progress_background_stroke_color',
        [
            'label' => __( 'Background Stroke Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [      
                  '{{WRAPPER}} .learndash-course-grid-widget .list-option .progress-back.ProgressBar-background' => 'background: {{VALUE}}',
                '{{WRAPPER}} .learndash-course-grid-widget-progress-style .ProgressBar-background' => 'stroke: {{VALUE}}',
            ],
            'default' => '#fff',
        ]
    );
    $this->add_control(
        'progress_value_stroke_color',
        [
            'label' => __( 'Value Stroke Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget .list-option .progress-strocke.ProgressBar-circle' => 'background: {{VALUE}}',
                '{{WRAPPER}} .learndash-course-grid-widget-progress-style .ProgressBar-circle' => 'stroke: {{VALUE}}',
            ],
            'default' => '#93013c',
        ]
    );
    $this->add_control(
        'progress_background_color',
        [
            'label' => __( 'Background Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .learndash-course-grid-widget-progress-style .ProgressBar-circle' => 'fill: {{VALUE}}',
            ],
            'default' => 'transparent',
        ]
    );
    $this->add_control(
        'progress_text_color',
        [
            'label' => __( 'Text Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .list-option .ProgressBar-percentage' => 'color: {{VALUE}}',
                '{{WRAPPER}} .learndash-course-grid-widget-progress-style .ProgressBar-percentage' => 'color: {{VALUE}}',
            ],
            'default' => '#fff',
        ]
    );
    //Progress Text Typography
    $this->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'progress_text_typography',
            'label' => __( 'Value Typography', 'plugin-domain' ),
            'selector' => '{{WRAPPER}} .learndash-course-grid-widget-progress-style .ProgressBar-percentage, {{WRAPPER}} .list-option .ProgressBar-percentage',
        ]
    );
        //content position
        $this->add_control(
        'progress_position',
        [
            'label' => esc_html__( 'Content Position', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::SELECT,
            'dynamic' => [
                'active' => true,
            ],
            'options' => [	
                'default' => esc_html__( 'Top Left', 'elementor-custom-widgets-for-learndash' ), 
            'bottom-left' => esc_html__( 'Bottom Left', 'elementor-custom-widgets-for-learndash' ), 
            'bottom-right' => esc_html__( 'Bottom Right', 'elementor-custom-widgets-for-learndash' ),
            'top-right' => esc_html__( 'Top Right', 'elementor-custom-widgets-for-learndash' ),
            ],
            'default' => 'default',
        ]
    );    
    $this->end_controls_section();
    $this->start_controls_section(
        'carousel_tab',
        [
            'label' => __( 'Carousel', 'elementor-custom-widgets-for-learndash' ),
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
        ]
    ); 
    $this->add_control(
        'heading_9',
        [
            'label' => __( 'Icon', 'elementor-custom-widgets-for-learndash' ),
            'type' => \Elementor\Controls_Manager::HEADING,  
        ]
    );
    $this->add_responsive_control(
        'icon_carousel_size',
        [
            'label' => __( 'Size', 'plugin-domain' ),
            'type' => Controls_Manager::SLIDER,
            'size_units' => [ 'px'  ],
            'description' => 'Default: 30px',
            'range' => [
                'px' => [
                    'min' => 0,
                    'max' => 60,
                    'step' => 1,
                ], 
            ],
            'default' => [
                'unit' => 'px',
                'size' => 26,
            ],
            'selectors' => [
                 '{{WRAPPER}} .owl-carousel .owl-nav button.owl-prev, {{WRAPPER}} .owl-carousel .owl-nav button.owl-next' => 'font-size: {{SIZE}}{{UNIT}};',
            ],
        ]
    );
      
    $this->add_control(
        'selected_icon_carousel_view',
        [
            'label' => esc_html__( 'Display ', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'default' => esc_html__( 'Default', 'elementor' ),
                'stacked' => esc_html__( 'Stacked', 'elementor' ),
                'framed' => esc_html__( 'Framed', 'elementor' ),
            ],
            'default' => 'framed',
            'prefix_class' => 'carousel-view-',
        ]
    );
    

    $this->add_control(
        'selected_icon_carousel_shape',
        [
            'label' => esc_html__( 'Shape', 'elementor' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'circle' => esc_html__( 'Circle', 'elementor' ),
                'square' => esc_html__( 'Square', 'elementor' ),
            ],
            'default' => 'circle',
            'condition' => [
                'selected_icon_carousel_view!' => 'default',
            ],
            'prefix_class' => 'carousel-shape-',
        ]
    );

    $this->add_control(
        'selected_icon_carousel_icon_color',
        [
            'label' => __( 'Primary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .owl-carousel .owl-nav button.owl-prev,
                 {{WRAPPER}} .owl-carousel .owl-nav button.owl-next,
                 {{WRAPPER}} .owl-theme .owl-dots .owl-dot.active span,
                 {{WRAPPER}} .owl-theme .owl-dots .owl-dot:hover span' => 'color: {{VALUE}}',
             ],
            'default' => '#93013c',
        ]
    );
    $this->add_control(
        'selected_icon_carousel_back_color',
        [
            'label' => __( 'Secondary Color', 'plugin-domain' ),
            'type' => \Elementor\Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .owl-carousel .owl-nav button.owl-prev, {{WRAPPER}} .owl-carousel .owl-nav button.owl-next' => 'background-color: {{VALUE}}',
            ],   
        ]
    ); 
    
    $this->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'carousel_border_width',
            'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
            'selector' => '{{WRAPPER}} .owl-carousel .owl-nav button.owl-prev, {{WRAPPER}} .owl-carousel .owl-nav button.owl-next',
            'condition' => [
                'selected_icon_carousel_view' => 'framed',
            ],
        ]
    );
        $this->add_responsive_control(
        'selected_icon_carousel_padding',
        [
            'label' => __( 'Padding', 'plugin-domain' ),
            'type' => Controls_Manager::DIMENSIONS, 
            
            'size_units' => [ 'px'  ], 
            'default' => [
                'top' => 3,
                'right' => 3,
                'bottom' => 3,
                'left' => 3,
            ],
            'condition' => [
                'selected_icon_carousel_view!' => 'default',
            ],
            'selectors' => [
                '{{WRAPPER}} .owl-carousel .owl-nav button.owl-prev, {{WRAPPER}} .owl-carousel .owl-nav button.owl-next ' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
            ],
        ]
    );
     
     
    $this->end_controls_section();
}
private function style_tab(){

}
protected function render(){
    $this->get_latest_user_activity();
    $settings = $this->get_settings_for_display();
    $course_id= $settings['course_id']; 
    $course_modules=$settings['hidden_course_all_modules']; 
    $user_last_lessons= $settings['hidden_user_activity'];
    $rand= $settings['hidden_rand_id'];
    $show_carousel= $settings['show_carousel'];
    $show_next_module= $settings['show_next_module'];
    $show_grid= $settings['show_grid'] ;
    $desktop_item_num= $settings['items_to_show_desktop'] ;
    $tablet_item_num= $settings['items_to_show_tablet'] ;
    $mobile_item_num= $settings['items_to_show_mobile'] ;

    ?>
     <div class="learndash-course-grid-widget">
        <?php if($show_grid == "yes"){ ?>
        <div class="carousel-option">
   
            <div class="<?php if( $show_carousel== "yes"){ echo "owl-carousel owl-theme"; } else{ echo "card-grid"; }?> lesson-carousel" id="lesson-carousel-<?php echo $rand; ?>" data-responsive='{"0":{"items":"<?php echo $mobile_item_num; ?>"},"768":{"items": "<?php echo $tablet_item_num ?>"}, "1074":{"items": "<?php echo $desktop_item_num; ?>"}}'  data-loop="true" data-navs="true" data-margin="15" >
           <?php  
             if($show_next_module == "yes"){  
            foreach($course_modules as $course_module){
                $short_excerpt= substr( $course_module['module_excerpt'],0, $settings['excerpt_lenght']); 
               if($course_module['course_id']== $course_id){  
                   if($course_module['module_id'] == $user_last_lessons[$course_id]){  
                        $data_step='start';
                    }  
                if( $data_step=='start'){
                     include  ELCW_PLUGIN_PATH_template.'/learndash-course-grid-card-template.php';     
                 }     
                 }
                } 
                foreach($course_modules as $course_module){
                    $short_excerpt= substr( $course_module['module_excerpt'],0, $settings['excerpt_lenght']); 
                   if($course_module['course_id']== $course_id){   
                   if($course_module['module_id'] == $user_last_lessons[$course_id]){  
                        $data_step='stop';
                    } 
                    if($course_module['module_id'] != $user_last_lessons[$course_id]){  
                         include  ELCW_PLUGIN_PATH_template.'/learndash-course-grid-card-template.php';     
                    }
                         if($data_step == 'stop'){   
                            break;
                        }
                     }
                  } 
            }else{
                foreach($course_modules as $course_module){
                    $short_excerpt= substr( $course_module['module_excerpt'],0, $settings['excerpt_lenght']); 
                   if($course_module['course_id']== $course_id){  
                         include  ELCW_PLUGIN_PATH_template.'/learndash-course-grid-card-template.php';     
                     }
                  }
            }
              ?>
        </div>
        <?php } else{ ?> 
        <div class="list-option">
            <?php
             foreach($course_modules as $course_module){
                $short_excerpt= substr( $course_module['module_excerpt'],0, $settings['excerpt_lenght']); 
               if($course_module['course_id']== $course_id){  
                     include  ELCW_PLUGIN_PATH_template.'/learndash-course-grid-list-template.php';     
                 }
              }
            ?>
        </div>
        <?php } ?>
    </div>

<?php
}
protected function _content_template() {
?> 
    
<#
var iconCompleted = elementor.helpers.renderIcon( view, settings.selected_icon_completed, { 'aria-hidden': true }, 'i' , 'object' ); 
var iconClosed = elementor.helpers.renderIcon( view, settings.selected_icon_closed, { 'aria-hidden': true }, 'i' , 'object' ); 
var iconIncompleted = elementor.helpers.renderIcon( view, settings.selected_icon_incomplete, { 'aria-hidden': true }, 'i' , 'object' ); 
var iconRight = elementor.helpers.renderIcon( view, settings.selected_carousel_icon_right, { 'aria-hidden': true }, 'i' , 'object' ); 
var iconLeft = elementor.helpers.renderIcon( view, settings.selected_carousel_icon_left, { 'aria-hidden': true }, 'i' , 'object' ); 
var course_modules=settings.hidden_course_all_modules ;  
var course_id=  settings.course_id; 
var rand= settings.hidden_rand_id; 
var user_last_lessons= settings.hidden_user_activity;  
var show_carousel= settings.show_carousel;
var show_next_module= settings.show_next_module;
var show_grid= settings.show_grid;
var desktop_item_num= settings.items_to_show_desktop;
var tablet_item_num= settings.items_to_show_tablet;
var mobile_item_num= settings.items_to_show_mobile;
let data_step = 'stop';
    #>
   
    <div class="learndash-course-grid-widget">
        <# if( settings.show_grid  == "yes"){ #>
        <div class="carousel-option">
            <div class="<# if(settings.show_carousel  == 'yes'){ #> owl-carousel owl-theme <# } else { #>  card-grid <# } #> lesson-carousel"  data-responsive='{"0":{"items":"{{mobile_item_num}}"},"768":{"items": "{{tablet_item_num}}"}, "1074":{"items": "{{desktop_item_num}}"}}'  id="lesson-carousel-{{rand}}" data-loop="true" data-navs="true" data-margin="15" >
             
            <# 
            if(show_next_module == 'yes'){
                _.each(course_modules, function (course_module) {
                    if (course_module.course_id == course_id) {
                        var short_excerpt = course_module.module_excerpt.substr(0, settings.excerpt_lenght);
                        if (course_module.module_id == user_last_lessons[course_id]) {
                             data_step = 'start';
                        }
                        if (data_step == 'start') { 
                            #>
                                <?php include ELCW_PLUGIN_PATH_template.
                            '/learndash-course-grid-card-template-frontend.php'; ?>
                            <#
                        }
                    }
                })
                try {
              _.each(course_modules , function(course_module){
                if(course_module.course_id ==  course_id){ 
                    let text= course_module.module_excerpt; 
                    if(text){
					 var short_excerpt= text.substr( 0,  settings.excerpt_lenght ); 
                    }
                    else{
                        var short_excerpt= "";   
                    }
                   if( course_module.module_id == user_last_lessons[course_id]){  
                        var data_step='stop';
                    } 
                    if( course_module.module_id != user_last_lessons[course_id]){  
                        
                 #>  
                <?php include  ELCW_PLUGIN_PATH_template.'/learndash-course-grid-card-template-frontend.php'; ?>
                <# 
                    }
                        if( data_step == 'stop'){   
                            throw 'Break';
                    } 
              } })
            }
            catch (e) {
            if (e !== 'Break') throw e
            } 
            }
            else{ 
                _.each(course_modules , function(course_module){
                if(course_module.course_id ==  course_id){
                    let text= course_module.module_excerpt; 
                    if(text){
					 var short_excerpt= text.substr( 0,  settings.excerpt_lenght ); 
                    }
                    else{
                        var short_excerpt= "";   
                    }
                    #>
                <?php include  ELCW_PLUGIN_PATH_template.'/learndash-course-grid-card-template-frontend.php'; ?>
                <#       
              } }) 
            } #>
             
        </div>
        <# } else{ #> 
        <div class="list-option">
    <#        _.each(course_modules , function(course_module){
                if(course_module.course_id ==  course_id){
                    let text= course_module.module_excerpt; 
                    if(text){
					 var short_excerpt= text.substr( 0,  settings.excerpt_lenght ); 
                    }
                    else{
                        var short_excerpt= "";   
                    } 
                    #>
                <?php include  ELCW_PLUGIN_PATH_template.'/learndash-course-grid-list-template-frontend.php'; ?>
                <#       
              } }) 
             #>    
    </div>
        <# } #>
    </div>
     
<?php
}
}
Plugin::instance()->widgets_manager->register_widget_type( new LEARNDASH_COURSE_GRID_WIDGET());