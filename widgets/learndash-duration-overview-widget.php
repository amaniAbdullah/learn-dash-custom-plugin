<?php
namespace Elementor;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
if (!defined('ABSPATH')) {
    $parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
    include_once $parse_uri[0] . '/wp-load.php';
}

namespace Elementor;

class PROGRESS_BAR_WIDGET extends Widget_Base
{

    public function get_precent($date1, $date2)
    {
        if (strtotime($date1) >= strtotime($date2)) {
            $newDate1 = date("d-m-Y", strtotime($date1));
            $newDate2 = date("d-m-Y", strtotime($date2));
        } else {
            $newDate2 = date("d-m-Y", strtotime($date1));
            $newDate1 = date("d-m-Y", strtotime($date2));
        }
         $datesDifferent = strtotime($newDate1) - strtotime($newDate2);
         $totalDays = round($datesDifferent / 86400);
         $todayDate = date("d-m-Y");
         if($totalDays > 0 && strtotime($todayDate) <= strtotime($newDate1) && strtotime($todayDate) >= strtotime($newDate2)){
                $daysNum= strtotime($todayDate) - strtotime($newDate2);
                $roundDaysNum = round($daysNum / 86400);
                $precent = ($roundDaysNum *100 ) / $totalDays;
                $leftDays = $totalDays - $roundDaysNum;
                $items =array(
                    'precent'=> $precent,
                    'totalDays'=> $totalDays,
                    'passedDays'=> $roundDaysNum,
                    'leftDays' => $leftDays
                );
            }
            else if($totalDays > 0 && strtotime($todayDate) <= strtotime($newDate1) && strtotime($todayDate) <= strtotime($newDate2)){
                $precent = 0;
                $leftDays = 0;
                $items =array(
                    'precent'=> $precent,
                    'totalDays'=> $totalDays,
                    'passedDays'=> '0',
                    'leftDays' => $leftDays
                );
            }
        else{
            $precent = 100;
            $leftDays = -1;
                $items =array(
                    'precent'=> $precent,
                    'totalDays'=> $totalDays,
                    'passedDays'=> '0',
                    'leftDays' => $leftDays
                );
        }
        return (array) $items;
    }
    public function get_name()
    {
        return 'learndash-duration-overview-widget';
    }
    public function get_title()
    {
        return esc_html__('Duration Overview', 'elementor-custom-widgets-for-learndash');
    }
    public function get_script_depends()
    {
        return [
            'elcw-script',
        ];
    }
    public function get_icon()
    {
        return 'eicon-skill-bar';
    }
    public function get_categories()
    {
        return [
            'learndash-custom-widgets-for-elementor',
        ];
    }

    public function _register_controls()
    {

        //Content Setings
        $this->start_controls_section(
            'progress_bar_setting',
            [
                'label' => __('Progress Bar', 'elementor-custom-widgets-for-learndash'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'date_field_1',
            [
                'label' => esc_html__('Start Date Field', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
                'placeholder' => esc_html__('Advance Custom Field', 'elementor-custom-widgets-for-learndash'),
            ]
        );

        $this->add_control(
            'date_field_2',
            [
                'label' => esc_html__('End Date Field', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
                'placeholder' => esc_html__('Advance Custom Field', 'elementor-custom-widgets-for-learndash'),
            ]
        );
        $this->add_control(
            'finish_text',
            [
                'label' => esc_html__('Finish Text', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
                'default' => __('Die Offizielle Dauer ist zu Ende', 'elementor-custom-widgets-for-learndash'),
                'placeholder' => esc_html__('Die Offizielle Dauer ist zu Ende', 'elementor-custom-widgets-for-learndash'),
            ]
        );
        $this->end_controls_section();
        $this->start_controls_section(
            'text_tab',
            [
                'label' => __('Text', 'elementor-custom-widgets-for-learndash'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__('Title', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
                'default' => __('Verbleibende Zeit', 'elementor-custom-widgets-for-learndash'),
                'placeholder' => esc_html__('Verbleibende Zeit', 'elementor-custom-widgets-for-learndash'),
            ]
        );
        $this->add_control(
            'tooltip',
            [
                'label' => esc_html__('Tooltip', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::WYSIWYG,
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
                'default' => __('Nach Ablauf der Zeit hast du keinen Zugriff mehr auf die Live Calls. Communitv und den', 'elementor-custom-widgets-for-learndash'),
                'placeholder' => __('Tooltip Text...', 'elementor-custom-widgets-for-learndash'),
            ]
        );
        $this->add_control(
            'initiale',
            [
                'label' => __('Set Custom Value', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'elementor-custom-widgets-for-learndash'),
                'label_off' => __('No', 'elementor-custom-widgets-for-learndash'),
                'return_value' => 'yes',
                'default' => 'No',
            ]
        );
        $this->end_controls_section();
        /**
         * start style tab
         */
        $this->start_controls_section(
            'progress_bar_style',
            [
                'label' => esc_html__('Progress Bar', 'elementor-custom-widgets-for-learndash'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'bar_background_type',
                'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash'),
                'types' => ['classic', 'gradient'],
                'exclude' => ['image'],
                'selector' => '{{WRAPPER}} .learndash-duration-overview-widget-bar-style',
                'fields_options' => [
                    'background' => [
                        'default' => 'classic',
                    ],
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'value_background_type',
                'label' => esc_html__('Value Background Type', 'elementor-custom-widgets-for-learndash'),
                'types' => ['classic', 'gradient'],
                'exclude' => ['image'],
                'selector' => '{{WRAPPER}} .learndash-duration-overview-widget-value-style',
                'fields_options' => [
                    'background' => [
                        'default' => 'classic',
                    ],
                ],
            ]
        );
        $this->add_responsive_control(
            'thickness',
            [
                'label' => __('Thickness', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 10,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'size' => 10,
                ],
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-widget-value-style' => 'height: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .learndash-duration-overview-widget-bar-style' => 'height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_responsive_control(
            'bar_padding',
            [
                'label' => __('Padding', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'size' => 10,
                ],
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-widget-bar-style' => 'margin-top: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_control(
            'progress_animation_duration',
            [
                'label' => __('Animation Duration', 'plugin-domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 5,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'size' => 2,
                ],
            ]
        );
        $this->add_control(
            'rounded_corner',
            [
                'label' => __('Rounded Edges', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __('Yes', 'elementor-custom-widgets-for-learndash'),
                'label_off' => __('No', 'elementor-custom-widgets-for-learndash'),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );
        $this->add_control(
            'heading_0_2',
            [
                'label' => __('Value Text', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'value_text_color',
            [
                'label' => __('Text Color', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-value-text' => 'color: {{VALUE}}',
                ],
                'default' => '#73c0b1',
            ]
        );
        //Title Text Typography
        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'value_text_typography',
                'label' => __('Typography', 'plugin-domain'),
                'selector' => '{{WRAPPER}} .learndash-duration-overview-value-text',
            ]
        );

        $this->add_control(
            'heading_0_3',
            [
                'label' => __('Finish Text', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'finish_text_color',
            [
                'label' => __('Text Color', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-finish-text' => 'color: {{VALUE}}',
                ],
                'default' => '#73c0b1',
            ]
        );
        //Title Text Typography
        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'finish_text_typography',
                'label' => __('Typography', 'plugin-domain'),
                'selector' => '{{WRAPPER}} .learndash-duration-overview-finish-text',
            ]
        );
        $this->end_controls_section();
        $this->start_controls_section(
            'section_description',
            [
                'label' => esc_html__('Text', 'elementor-custom-widgets-for-learndash'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        // Title Color
        $this->add_control(
            'heading-1',
            [
                'label' => __('Title', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HEADING,
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} h3.learndash-duration-overview-title-text' => 'color: {{VALUE}}',
                ],
                'default' => 'black',
            ]
        );
        //Title Text Typography
        $this->add_group_control(
            \Elementor\Group_Control_Typography::get_type(),
            [
                'name' => 'title_text_typography',
                'label' => __('Typography', 'plugin-domain'),
                'selector' => '{{WRAPPER}} h3.learndash-duration-overview-title-text',
            ]
        );
        $this->add_control(
            'heading-2',
            [
                'label' => __('Icon', 'elementor-custom-widgets-for-learndash'),
                'type' => \Elementor\Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        $this->add_responsive_control(
            'icon_size',
            [
                'label' => __('Size', 'plugin-domain'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'description' => 'Default: 20px',
                'range' => [
                    'px' => [
                        'min' => 20,
                        'max' => 80,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 20,
                ],
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-widget-icon-style i,
					 {{WRAPPER}} learndash-duration-overview-widget-icon-style svg' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_responsive_control(
            'icon_left_margin',
            [
                'label' => __('Left Margin', 'plugin-domain'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'description' => 'Default: 20px',
                'range' => [
                    'px' => [
                        'min' => 10,
                        'max' => 60,
                        'step' => 1,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 10,
                ],
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-widget-icon-style' => 'margin-left: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_responsive_control(
            'icon_rotation',
            [
                'label' => __('Rotation', 'plugin-domain'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['deg'],
                'default' => [
                    'size' => 0,
                    'unit' => 'deg',
                ],
                'tablet_default' => [
                    'unit' => 'deg',
                ],
                'mobile_default' => [
                    'unit' => 'deg',
                ],
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-widget-icon-style i,
					{{WRAPPER}} learndash-duration-overview-widget-icon-style svg' => 'transform: rotate({{SIZE}}{{UNIT}});',
                ],
            ]
        );
        $this->add_control(
            'selected_icon',
            [
                'label' => esc_html__('Icon', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::ICONS,
                'fa4compatibility' => 'icon',
                'default' => [
                    'value' => 'fas fa-info-circle',
                    'library' => 'fa-solid',
                ],
            ]
        );
        $this->add_control(
            'selected_icon_view',
            [
                'label' => esc_html__('Display ', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'default' => esc_html__('Default', 'elementor-custom-widgets-for-learndash'),
                    'stacked' => esc_html__('Stacked', 'elementor-custom-widgets-for-learndash'),
                    'framed' => esc_html__('Framed', 'elementor-custom-widgets-for-learndash'),
                ],
                'default' => 'default',
                'prefix_class' => 'duration-overview-info-',
            ]
        );

        $this->add_control(
            'selected_icon_shape',
            [
                'label' => esc_html__('Shape', 'elementor-custom-widgets-for-learndash'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'circle' => esc_html__('Circle', 'elementor-custom-widgets-for-learndash'),
                    'square' => esc_html__('Square', 'elementor-custom-widgets-for-learndash'),
                ],
                'default' => 'circle',
                'condition' => [
                    'selected_icon_view!' => 'default',
                ],
                'prefix_class' => 'duration-overview-info-',
            ]
        );

        $this->add_control(
            'selected_icon_color',
            [
                'label' => __('Primary Color', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-widget-icon-style i' => 'color: {{VALUE}}',
                    '{{WRAPPER}} .learndash-duration-overview-widget-icon-style svg' => 'fill: {{VALUE}}',
                ],
                'default' => '#5F5F5F',
            ]
        );
        $this->add_control(
            'selected_icon_back_color',
            [
                'label' => __('Secondary Color', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-widget-icon-style i,{{WRAPPER}} .learndash-duration-overview-widget-icon-style svg' => 'background-color: {{VALUE}}',
                ],
                'condition' => [
                    'selected_icon_view!' => 'default',
                ],
            ]
        );

        $this->add_group_control(
            \Elementor\Group_Control_Border::get_type(),
            [
                'name' => 'border_width',
                'label' => __('Border', 'elementor-custom-widgets-for-learndash'),
                'selector' => '{{WRAPPER}} .learndash-duration-overview-widget-icon-style i, {{WRAPPER}} .learndash-duration-overview-widget-icon-style svg',
                'condition' => [
                    'selected_icon_view' => 'framed',
                ],
            ]
        );
        $this->add_responsive_control(
            'selected_icon_padding',
            [
                'label' => __('Padding', 'plugin-domain'),
                'type' => Controls_Manager::DIMENSIONS,

                'size_units' => ['px'],
                'default' => [
                    'top' => 3,
                    'right' => 3,
                    'bottom' => 3,
                    'left' => 3,
                ],
                'condition' => [
                    'selected_icon_view!' => 'default',
                ],
                'selectors' => [
                    '{{WRAPPER}} .learndash-duration-overview-widget-icon-style i, {{WRAPPER}} .learndash-duration-overview-widget-icon-style svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        $duration_info = $this-> get_precent($settings['date_field_1'], $settings['date_field_2']);
        $precent = $duration_info['precent'];
        $taskTotalDays = $duration_info['totalDays'];
        $taskPassedDays = $duration_info['passedDays'];
        $taskLeftDays = $duration_info['leftDays'];
        $rounded_corner = $settings['rounded_corner'] == "yes" ? 'border-raduis' : '';
        include ELCW_PLUGIN_PATH_template . '/learndash-duration-overview-template.php';

    }

    protected function _content_template()
    {?>
		<#
		var rounded_corner= settings.rounded_corner == 'yes' ? 'border-raduis' : '';
		var selectedIcon = elementor.helpers.renderIcon( view, settings.selected_icon, { 'aria-hidden': true }, 'i' , 'object' );
		#>
		<?php include ELCW_PLUGIN_PATH_template . '/learndash-duration-overview-frontend-template.php';
    }
}
Plugin::instance()->widgets_manager->register_widget_type(new PROGRESS_BAR_WIDGET());
