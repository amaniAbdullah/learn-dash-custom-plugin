<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
if ( ! defined( 'ABSPATH' ) ) {
	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	include_once $parse_uri[0] . '/wp-load.php';
}

namespace Elementor;
class CIRCLE_PROGRESS_WIDGET extends Widget_Base{

     public function get_current_course(){
	    global $wp_query;
		$current_page_id= get_the_ID();
		$current_page_post_type=get_post_type($current_page_id);
		 $items=array();
		 $items =array(
				'current_page_post_type'=> $current_page_post_type,
				'page_id'=> $current_page_id,
			);
        return (array) $items;

	}
	public function get_courses_progress(){
	    $user_ID = get_current_user_id();
		global $wpdb;
	    $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
		$items=array();
		foreach($courses as $course){
			$total_steps= learndash_get_course_steps_count($course->ID);
			$completed_steps= learndash_course_get_completed_steps($user_ID,$course->ID);
            if($total_steps != 0){
		  $user_progress= number_format($completed_steps* 100 / $total_steps,0,'.','');
            }
            else{
                $user_progress = 0;
            }
			$items[ ]= array(
				'course_id'=>$course->ID,
				'user_course_progress'=>$user_progress
			);
		 }
        return (array) $items;

	}
	public function get_courses(){
		global $wpdb;
		$courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
		$items=array();
		foreach($courses as $course){
			$items[$course->ID ]= $course->post_title;
		}

		return (array) $items;

	}

    public function get_name(){
        return 'learndash-circle-progress-widget';
    }
    public function get_title(){
        return esc_html__('Circle Progress','elementor-custom-widgets-for-learndash');
    }
    public function get_script_depends(){
        return[
            'elcw-script'
        ];
    }
    public function get_icon(){
        return 'eicon-counter-circle';
    }
    public function get_categories(){
        return[
            'learndash-custom-widgets-for-elementor'
        ];
    }


    public function _register_controls() {

        //Content Setings
		$this->start_controls_section(
            'course_setting',
            [
                'label'=>__('Course Setting','elementor-custom-widgets-for-learndash'),
                'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
		$this->add_control(
			'hidden_course_progress',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => $this->get_courses_progress(),
			]
		);
		$this->add_control(
			'hidden_current_page',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => $this->get_current_course(),
			]
		);
		$this->add_control(
			'course_option',
			[
				'label' => esc_html__( 'Course Options', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SELECT,
                'dynamic' => [
					'active' => true,
				],
				'options' => [
                    'current' => esc_html__( 'Current Course', 'elementor-custom-widgets-for-learndash' ),
                'spacific' => esc_html__( 'Specific Course', 'elementor-custom-widgets-for-learndash' ),
				],
				'default' => 'spacific',
			]
		);

		$this->add_control(
			'course_id',
			[
				'label' => __( 'Course', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => false,
				'options' =>  $this->get_courses(),
				'condition' => [
					'course_option' => 'spacific'
				]
			]
		);


        $this->end_controls_section();
        $this->start_controls_section(
            'description_section',
            [
                'label'=>__('Description','elementor-custom-widgets-for-learndash'),
                'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
			'title',
			[
				'label' =>  esc_html__( 'Title', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
					'active' => true,
				],
				'default' => __( 'Title of the circle progress', 'elementor-custom-widgets-for-learndash' ),
				'placeholder' => esc_html__( 'Title of the circle progress', 'elementor-custom-widgets-for-learndash' ),
			]
		);
        $this->add_control(
			'subtitle',
			[
				'label' =>  esc_html__( 'Subtitle', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
					'active' => true,
				],
				'default' => __( 'Subtitle of the progress', 'elementor-custom-widgets-for-learndash' ),
				'placeholder' => __( 'Subtitle of the progress', 'elementor-custom-widgets-for-learndash' ),
			]
		);

        $this->add_control(
			'label_position',
			[
				'label' => esc_html__( 'Label Position', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SELECT,
                'dynamic' => [
					'active' => true,
				],
				'options' => [
                    'default' => esc_html__( 'Bottom', 'elementor-custom-widgets-for-learndash' ),
                'top' => esc_html__( 'Top', 'elementor-custom-widgets-for-learndash' ),
				],
				'default' => 'default',
			]
		);
        $this->add_control(
			'link_subtitle',
			[
				'label' => esc_html__( 'Link Subtitle', 'elementor' ),
				'type' => Controls_Manager::URL,
				'dynamic' => [
					'active' => true,
				],
				'default' => [
					'url' => '',
				],
                'placeholder' => __( 'http://dein-link.de/', 'elementor-custom-widgets-for-learndash' ),

			]
		);
        $this->end_controls_section();
        $this->start_controls_section(
            'value_section',
            [
                'label'=>__('Value','elementor-custom-widgets-for-learndash'),
                'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control(
			'set_value',
			[
				'label' => __( 'Set Custom Value', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'No', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'No',
			]
		);
        $this->add_control(
			'value_number_prefix',
			[
				'label' => __( 'Value Number Prefix', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 0,
				'max' => 100,
				'step' => 1,
                'condition' => [
					'set_value' => 'yes',
				],
			]
		);
        $this->add_control(
			'value_number_suffix',
			[
				'label' =>  esc_html__( 'Value Number Suffix', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::TEXT,
                'dynamic' => [
					'active' => true,
				],
                'condition' => [
					'set_value' => 'yes',
				],
				'default' => __( '%', 'elementor-custom-widgets-for-learndash' ),
				'placeholder' => esc_html__( '%', 'elementor-custom-widgets-for-learndash' ),
			]
		);
        $this->add_control(
			'show_value',
			[
				'label' => __( 'Show Value', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);

        $this->end_controls_section();
        /**
		 * start style tab
		 */
		$this->start_controls_section(
			'section_circle_progress',
			[
				'label' => esc_html__( 'Circle Progress', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'heading_0_1',
			[
				'label' => __( 'Background Stroke Type', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
		);
		$this->add_control(
			'background_stroke_value_color',
			[
				'label' => __( 'Background Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-circle-progress-back-stroke-style #Gradient2 .stop-1' => 'stop-color: {{VALUE}}',
				],
				'default' => '#2fb76e',
			]
		);
		$this->add_control(
			'background_stroke_value_second_color',
			[
				'label' => __( 'Background Second Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-circle-progress-back-stroke-style #Gradient2 .stop-2' => 'stop-color: {{VALUE}}',
				],
				'default' => '#2fb76e',
			]
		);
		$this->add_control(
			'heading_0_2',
			[
				'label' => __( 'Value Stroke Type', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
			$this->add_control(
				'background_stroke_back_color',
				[
					'label' => __( 'Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .learndash-circle-progress-back-stroke-style #Gradient1 .stop-1' => 'stop-color: {{VALUE}}',
					],
					'default' => '#00233d',
				]
			);
			$this->add_control(
				'background_stroke_back_second_color',
				[
					'label' => __( 'Background Second Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .learndash-circle-progress-back-stroke-style #Gradient1 .stop-2' => 'stop-color: {{VALUE}}',
					],
					'default' => '#00233d',
				]
			);
		$this->add_control(
			'circle_size',
			[
				'label' => __( 'Circle Size', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'separator'=>'before',
				'range' => [
					'%' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'default' => [
					'size' =>40
				],
				'selectors' => [
					'{{WRAPPER}} .learndash-circle-progress-widget-circle-style' => 'width: {{SIZE}}%;',
				],

			]
		);
		$this->add_control(
			'background_stroke_width',
			[
				'label' => __( 'Background Stroke Width', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 40,
						'step' => 1,
					],
				],
				'default' => [
					'size' => 20,
				],
				'selectors' => [
					'{{WRAPPER}} .circle-progress .learndash-circle-progress-widget-circle-style .ProgressBar-circle' => 'stroke-width: {{SIZE}}px;',
				],

			]
		);
		$this->add_control(
			'value_stroke_width',
			[
				'label' => __( 'Background Stroke Width', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
						'px' => [
							'min' => 0,
							'max' => 40,
							'step' => 1,
						],
					],
					'default' => [
						'size' => 20,
					],
					'selectors' => [
						'{{WRAPPER}} .circle-progress .learndash-circle-progress-widget-circle-style .ProgressBar-background' => 'stroke-width: {{SIZE}}px;',
					],

			]
		);
		$this->add_control(
			'value_stroke_position',
			[
				'label' => __( 'Value Stroke Position', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 95,
						'step' => 1,
					],
				],
				'default' => [
					'size' => 85,
				],
			]
		);
		$this->add_control(
			'stroke_back_position',
			[
				'label' => __( 'Background Stroke Position', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 95,
						'step' => 1,
					],
				],
				'default' => [
					'size' => 85,
				],
			]
		);
		$this->add_control(
			'value_stroke_linecap',
			[
				'label' => esc_html__( 'Value Stroke Linecap', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SELECT,
                'dynamic' => [
					'active' => true,
				],
				'options' => [
                'butt' => esc_html__( 'Butt', 'elementor-custom-widgets-for-learndash' ),
                'rounded' => esc_html__( 'Rounded', 'elementor-custom-widgets-for-learndash' ),
			 ],
				'default' => 'butt',
			]
		);
		$this->add_control(
			'progress_animation_duration',
			[
				'label' => __( 'Animation Duration', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 5,
						'step' => 1,
					],
				],
				'default' => [
					'size' => 2,
				],
			]
		);

		$this->end_controls_section();
		$this->start_controls_section(
			'section_description',
			[
				'label' => esc_html__( 'Description', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		// Title Color
		$this->add_control(
			'heading-1',
			[
				'label' => __( 'Title', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-circle-progress-widget-circle-title-style h2' => 'color: {{VALUE}}',
				],
				'default' => '#485157',
			]
		);
		//Title Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_text_typography',
				'label' => __( 'Typography', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .learndash-circle-progress-widget-circle-title-style h2',
			]
		);
		$this->add_control(
			'heading-2',
			[
				'label' => __( 'Subtitle', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
		);
		// Subitle Color
		$this->add_control(
			'subtitle_color',
			[
				'label' => __( 'Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-circle-progress-widget-circle-subtitle-style h3' => 'color: {{VALUE}}',
				],
				'default' => '#485157',
			]
		);
		//Subitle Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'subtitle_text_typography',
				'label' => __( 'Typography', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .learndash-circle-progress-widget-circle-subtitle-style h3',
			]
		);

		$this->end_controls_section();
		$this->start_controls_section(
			'section_value',
			[
				'label' => esc_html__( 'Value', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'heading-3',
			[
				'label' => __( 'Progress Value', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
		);
			// Progress Value Color
			$this->add_control(
				'progress_value_color',
				[
					'label' => __( ' Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .learndash-circle-progress-widget-circle-text-style span' => 'color: {{VALUE}}',
					],
					'default' => '#2fb76e',
				]
			);
			//Progress Value Typography
			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'progress_value_typography',
					'label' => __( 'Typography', 'plugin-domain' ),
					'selector' => '{{WRAPPER}} .learndash-circle-progress-widget-circle-text-style span',
				]
			);
			$this->add_control(
				'heading-4',
				[
					'label' => __( 'Prefix', 'elementor-custom-widgets-for-learndash' ),
					'type' => \Elementor\Controls_Manager::HEADING,
				]
			);
			// Progress prefix Color
			$this->add_control(
				'progress_prefix_color',
				[
					'label' => __( 'Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .learndash-circle-progress-widget-circle-text-style span.prefix' => 'color: {{VALUE}}',
					],
				]
			);
			//Progress prefix Typography
			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'progress_prefix_typography',
					'label' => __( 'Typography', 'plugin-domain' ),
					'selector' => '{{WRAPPER}} .learndash-circle-progress-widget-circle-text-style span.prefix',
				]
			);
			$this->add_control(
				'heading-5',
				[
					'label' => __( 'Suffix', 'elementor-custom-widgets-for-learndash' ),
					'type' => \Elementor\Controls_Manager::HEADING,
				]
			);
			// Progress Suffix Color

			$this->add_control(
				'progress_suffix_color',
				[
					'label' => __( 'Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .learndash-circle-progress-widget-circle-text-style span.suffix' => 'color: {{VALUE}}',
					],
				]
			);
			//Progress Suffix Typography
			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name' => 'progress_suffix_typography',
					'label' => __( 'Typography', 'plugin-domain' ),
					'selector' => '{{WRAPPER}} .learndash-circle-progress-widget-circle-text-style span.suffix',
				]
			);
		$this->end_controls_section();

    }

	protected function render(){
		$settings = $this->get_settings_for_display();
		$target = $settings['link_subtitle']['is_external'] ? ' target="_blank"' : '';
		$nofollow = $settings['link_subtitle']['nofollow'] ? ' rel="nofollow"' : '';
		$precentageValue= $settings['value_number_prefix'];
		$choosen_type= $settings['course_option'];
		$courses_progress= $settings['hidden_course_progress'];
		$current_page_datas= $settings['hidden_current_page'];
		$current_page_id= $current_page_datas['page_id'];
		$current_page_type= $current_page_datas['current_page_post_type'];
		if($choosen_type == 'current'){
		foreach($courses_progress as $course_progress){
				if($current_page_type == 'sfwd-courses'){
					if($course_progress['course_id'] ==$current_page_id ){
					 	include  ELCW_PLUGIN_PATH_template.'/learndash-circle-progress-template.php';
					}

				}
			}
		}
			else{
		foreach($courses_progress as $course_progress){
				if($course_progress['course_id'] ==$settings['course_id'] ){
					include  ELCW_PLUGIN_PATH_template.'/learndash-circle-progress-template.php';
				}
			}
		}


		?>

	<?php }

    protected function _content_template() {
		?>
		<#
		var target = settings.link_subtitle.is_external ? ' target="_blank"' : '';
		var nofollow = settings.link_subtitle.nofollow ? ' rel="nofollow"' : '';
		var precentageValue= settings.value_number_prefix;
		var choosen_type= settings.course_option;
		var courses_progress= settings.hidden_course_progress;
		var choosen_type=  settings.course_option;
		var courses_progress=  settings.hidden_course_progress;
		var current_page_datas=  settings.hidden_current_page;
			var current_page_id= current_page_datas.page_id;
			var current_page_type= current_page_datas.current_page_post_type;

				if( choosen_type == 'current'){
				_.each( courses_progress , function(course_progress){
					if( current_page_type == 'sfwd-courses'){

						if( course_progress.course_id ==  current_page_id){

						#> <?php
							include  ELCW_PLUGIN_PATH_template.'/learndash-circle-progress-frontend-template.php';
					?>
				<#        }
						}

					})
				}
				else{
					_.each( courses_progress , function(course_progress){
						if( course_progress.course_id == settings.course_id ){ #>
						<?php
						include  ELCW_PLUGIN_PATH_template.'/learndash-circle-progress-frontend-template.php';
					?>
				<#  	}
					})
				}
		#>

	<?php
    }
}
Plugin::instance()->widgets_manager->register_widget_type( new CIRCLE_PROGRESS_WIDGET());