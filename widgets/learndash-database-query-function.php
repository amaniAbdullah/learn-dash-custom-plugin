<?php
 function get_courses(){
		global $wpdb; 
        $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
         $items=array();
        foreach($courses as $course){     
			$items[$course->ID ]= $course->post_title;
        }   
        return (array) $items;	 

	}
     function get_course_modules(){ 
        $lessons=learndash_get_course_lessons_list($settings['course_id']);
        global $wpdb; 
        $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
        $modules = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-lessons'");
        $items=array();
        $CourseModules=array();
        $i=0;
        foreach($courses as $course){      
                $items[]= array(
                    'course_id'=>$course->ID,
                    'course_step'=>json_encode(get_post_meta($course->ID, 'ld_course_steps'))
                );  
                  foreach($modules as $module){
                    if(strpos($items[$i]['course_step'], $module->ID) !== false){ 
                      $CourseModules[]=array(
                        'course_id'=>$course->ID,
                        'module_id'=>$module->ID,
                        'module_name'=>$module->post_name,
                        'module_title'=>$module->post_title
                    );
                    }
                     } 
               
                $i++;
                }
                  
        return (array) $CourseModules;
    }
     function get_course_modules_lessons(){
        global $wpdb; 
        $courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
        $modules = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-lessons'");
        $lessons = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type`='sfwd-topic'");
        $CourseLessons= array();
        $items=array();
        $CourseModules=array();
        $i=0;
        $x=0;
        $user_ID = get_current_user_id();
        $meta= array();
        foreach($courses as $course){      
                $items[]= array(
                    'course_id'=>$course->ID,
                    'course_step'=> array(get_post_meta($course->ID, 'ld_course_steps'))
                );    
              
                 foreach($modules as $module){
                    $Coursecount = count(reset($items)); 
                        $moduleTopic= $items[$i]['course_step'][0][0]['steps']['h']['sfwd-lessons'][$module->ID]['sfwd-topic'] ;
                        if($moduleTopic != NULL){
                            $keys= array_keys($moduleTopic); 
                            foreach ($keys as $key){
                                $topics_status=$wpdb->get_results("SELECT * FROM `".$wpdb->prefix."learndash_user_activity` WHERE `user_id` = $user_ID AND `course_id` = $course->ID AND `post_id` = $key");
                               if(count($topics_status)>0){
                                foreach($topics_status as $topic_status){
                                 
                                    if($topic_status->activity_completed != NULL){
                                        $topic_user_status="completed";
                                    }
                                    else if($topic_status->activity_completed == NULL){
                                        $topic_user_status="incomplete";
                                    }   
                                $meta[]=array( 
                                'topic_status'=>get_post_status($key),
                                'topic_title'=>get_the_title($key),
                                'topic_id'=>$key, 
                                'topic_user_status'=>$topic_user_status,
                                'module_id'=>$module->ID,
                                'course_id'=>$course->ID, 
                                'module_title'=>get_the_title($module->ID),
                                'course_title'=>get_the_title($course->ID),
                                );  
                            }
                        }
                        else{
                            $meta[]=array( 
                                'topic_status'=>get_post_status($key),
                                'topic_title'=>get_the_title($key),
                                'topic_id'=>$key, 
                                'topic_user_status'=>'no-activities',
                                'module_id'=>$module->ID,
                                'course_id'=>$course->ID, 
                                'module_title'=>get_the_title($module->ID),
                                'course_title'=>get_the_title($course->ID),
                                ); 
                        }
                          } 
                        } 
                     }   
                $i++;
                }       
       return (array) $meta;
    }