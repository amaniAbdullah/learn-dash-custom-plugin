<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
if ( ! defined( 'ABSPATH' ) ) {
	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	include_once $parse_uri[0] . '/wp-load.php';
}

define( 'WP_USE_THEMES', true );
class LEARNDASH_LESSON_CONTINUE_WIDGET extends Widget_Base{
	public function get_courses(){
		global $wpdb;
		$courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
		$items=array();
		foreach($courses as $course){
			$items[$course->ID ]= $course->post_title;
		}
		return (array) $items;
	}
	public function get_latest_user_activity(){
		global $wpdb;
		$courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
		$meta= array();
		foreach($courses as $course){
			$user_id = get_current_user_id();
		$last_activity=$wpdb->get_results("SELECT * FROM `".$wpdb->prefix."learndash_user_activity` WHERE `user_id` = $user_id AND `course_id` = $course->ID AND `activity_status` = 1  ORDER BY `activity_id` DESC LIMIT 1");
		foreach($last_activity as $activity){
			$meta[$course->ID] = $activity->post_id;
		}
	 }
	 return (array) $meta;
	}
	public function get_course_all_modules(){
		global $wpdb;
		$courses = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."posts` WHERE `post_type` = 'sfwd-courses'");
		$meta= array();
		foreach($courses as $course){
			$user_id = get_current_user_id();
			$lessons=$this->get_latest_user_activity();
			$last_post_ID = $lessons[$course->ID];
			$post_type= get_post_type($last_post_ID);
			$excerpt=  get_post_meta($last_post_ID, '_learndash_course_grid_short_description', true );
			$post_thumb= get_the_post_thumbnail_url($last_post_ID, 'full' );
			if($post_thumb != NUll){
				$thumb= $post_thumb;
			}
			else{
				$thumb= ELCW_PLUGIN_PATH_image. '\topic_thumbnail.svg';
			}
			$post_data= get_post($last_post_ID);
			$post_premalink= learndash_get_step_permalink($last_post_ID);
			if($last_post_ID != NULL){
				$meta[]=array(
					'course_id'=>$course->ID,
					'course_name'=>$course->post_title,
					'module_id'=>$last_post_ID,
					'module_name'=>$post_data->post_name,
					'module_title'=>$post_data->post_title,
					'module_excerpt'=>$excerpt,
					'module_premalink'=>$post_premalink,
					'status'=>'completed',
					'steps'=> 100,
					'image_url'=>$thumb,
					'post_status'=>$post_data->post_status
				);
			}
			else{
				$meta[]=array(
					'course_id'=>$course->ID,
					'course_name'=>'',
					'module_id'=>$last_post_ID,
					'module_name'=>'',
					'module_title'=>'',
					'module_excerpt'=>'',
					'module_premalink'=>'',
					'status'=>'completed',
					'steps'=> '',
					'image_url'=>'',
					'post_status'=>''
				);
			}

        }
		return (array) $meta;
	}
    public function get_name(){
        return 'learndash-lesson-continue-widget';
    }
    public function get_title(){
        return esc_html__('Activity','elementor-custom-widgets-for-learndash');
    }
    public function get_script_depends(){
        return[
            'elcw-script'
        ];
    }
    public function get_icon(){
        return 'eicon-checkbox';
    }
    public function get_categories(){
        return[
            'learndash-custom-widgets-for-elementor'
        ];
    }
    public function _register_controls() {
        //Content Setings
        $this->start_controls_section(
            'content_section',
            [
                'label'=>__('Content','elementor-custom-widgets-for-learndash'),
                'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
		$this->add_control(
			'hidden_user_activity',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => $this->get_latest_user_activity(),
			]
		);
		$this->add_control(
			'hidden_course_all_modules',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => $this->get_course_all_modules(),
			]
		);
		$this->add_control(
			'course_id',
			[
				'label' => __( 'Course', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => false,
				'options' =>  $this->get_courses(),
			]
		);
		$this->add_control(
			'show_media',
			[
				'label' => __( 'Media', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_control(
			'show_post_title',
			[
				'label' => __( 'Post Title', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_control(
			'show_course_title',
			[
				'label' => __( 'Course Title', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_control(
			'show_excerpt',
			[
				'label' => __( 'Excerpt', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_control(
			'show_icon',
			[
				'label' => __( 'Play Icon', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_control(
			'show_progress_bar',
			[
				'label' => __( 'Progress Bar', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_control(
			'show_start_btn',
			[
				'label' => __( 'Start Button', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
	    $this->add_control(
			'button_text',
			[
				'label' =>  esc_html__( 'Link Text', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
					'active' => true,
				],
				'default' => __( 'jetzt Ansehen', 'elementor-custom-widgets-for-learndash' ),
				'placeholder' => __( 'jetzt Ansehen', 'elementor-custom-widgets-for-learndash' ),
			]
		);
        $this->add_control(
			'layout_style',
			[
				'label' => esc_html__( 'Layout Style', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SELECT,
                'dynamic' => [
					'active' => true,
				],
				'options' => [
                    'full-size-layout' => esc_html__( 'Full Size', 'elementor-custom-widgets-for-learndash' ),
                'left-layout' => esc_html__( 'Left', 'elementor-custom-widgets-for-learndash' ),
				'right-layout' => esc_html__( 'Right', 'elementor-custom-widgets-for-learndash' ),
				],
				'default' => 'full-size-layout',
			]
		);
        $this->end_controls_section();
		//Content Setings
        $this->start_controls_section(
            'empty_content_section',
            [
                'label'=>__('Empty Activity','elementor-custom-widgets-for-learndash'),
                'tab'=> \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
		$this->add_control(
			'empty_title',
			[
				'label' =>  esc_html__( 'Title', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
					'active' => true,
				],
				'default' => __( 'jetzt Ansehen', 'elementor-custom-widgets-for-learndash' ),
				'placeholder' => __( 'jetzt Ansehen', 'elementor-custom-widgets-for-learndash' ),
			]
		);
		$this->add_control(
			'empty_button',
			[
				'label' =>  esc_html__( 'Button', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
					'active' => true,
				],
				'default' => __( 'jetzt Ansehen', 'elementor-custom-widgets-for-learndash' ),
				'placeholder' => __( 'jetzt Ansehen', 'elementor-custom-widgets-for-learndash' ),
			]
		);
		$this->add_control(
			'empty_button_link',
			[
				'label' =>  esc_html__( 'Button link', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::TEXT,
                'label_block' => true,
                'dynamic' => [
					'active' => true,
				],
				'default' => __( 'jetzt Ansehen', 'elementor-custom-widgets-for-learndash' ),
				'placeholder' => __( 'jetzt Ansehen', 'elementor-custom-widgets-for-learndash' ),
			]
		);
		$this->add_control(
			'empty_image',
			[
				'label' => esc_html__( 'Image', 'plugin-name' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
        $this->end_controls_section();
        //style tab
		$this->start_controls_section(
			'box_tab',
			[
				'label' => __( 'Box', 'elementor-custom-widgets-for-learndash' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'box_border',
				'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-box-style',
			]
		);
		$this->add_responsive_control(
			'border_box_radius',
			[
				'label' => __( 'Border Radius', 'elementor-custom-widgets-for-learndash' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'top' => 10,
					'right' => 10,
					'bottom' => 10,
					'left' => 10,
				],
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-box-style' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			\Elementor\Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
				'label' => __( 'Box Shadow', 'elementor-custom-widgets-for-learndash' ),
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-box-style',
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'box_background_type',
				'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-box-style .box-back',
				'fields_options' => [
					'background' => [
						'default' => 'classic',
					],
				],
			]
		);
		$this->add_control(
			'box_height',
			[
				'label' => esc_html__( 'Height', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'dynamic' => [
					'active' => true,
				],
				'options' => [
                    'default' => esc_html__( 'Srandard', 'elementor-custom-widgets-for-learndash' ),
                'fit' => esc_html__( 'Fit to screen', 'elementor-custom-widgets-for-learndash' ),
				'custom' => esc_html__( 'Min.height', 'elementor-custom-widgets-for-learndash' ),
				],
				'default' => 'default',
			]
		);
		$this->add_responsive_control(
			'box_custom_height',
			[
				'label' => __( 'Custom Height', 'elementor-custom-widgets-for-learndash' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'description' => 'Default: 300px',
				'range' => [
					'px' => [
						'min' => 434,
						'max' => 1000,
						'step' => 1,
					],
					'vh' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 434,
				],
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-box-style' => 'height: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'box_height' => 'custom'
				]
			]
		);


        $this->end_controls_section();
        $this->start_controls_section(
            'media_style_section',
            [
                'label' => __( 'Media', 'elementor-custom-widgets-for-learndash' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
		$this->add_control(
			'heading',
			[
				'label' => __( 'Media Overlay', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
		);

    $this->start_controls_tabs(
        'media_style_section_tabs'
    );
        // Normal State
        $this->start_controls_tab(
            'media_overlay_background_normal_state',
            [
                'label' => __( 'Normal', 'elementor-custom-widgets-for-learndash' ),
            ]
        );

            $this->add_group_control(
                Group_Control_Background::get_type(),
                [
                    'name' => 'media_overlay_background_normal',
                    'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash' ),
                    'types' => [ 'classic', 'gradient' ],
                    'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-overlay-style',
                    'fields_options' => [
                        'background' => [
                            'default' => 'classic',
                        ],
                    ],
                ]
            );

        $this->end_controls_tab();

        // Hover State
        $this->start_controls_tab(
            'media_overlay_background_hover_state',
            [
                'label' => __( 'Hover', 'elementor-custom-widgets-for-learndash' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'media_overlay_background_hover',
                'label' => esc_html__('Background Type', 'elementor-custom-widgets-for-learndash' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget .item:hover .learndash-lasson-continue-widget-overlay-style',
                'fields_options' => [
                    'background' => [
                        'default' => 'classic',
                    ],
                ],
            ]
        );
            $this->end_controls_tabs();
            $this->add_control(
                'heading_2',
                [
                    'label' => __( 'Play Icon', 'elementor-custom-widgets-for-learndash' ),
                    'type' => \Elementor\Controls_Manager::HEADING,
                    'separator' => 'before',
                ]
            );
            $this->add_responsive_control(
				'icon_size',
				[
					'label' => __( 'Size', 'plugin-domain' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px'  ],
					'description' => 'Default: 20px',
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 30,
					],
					'selectors' => [
						'{{WRAPPER}} .learndash-lasson-continue-widget-icon-style' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$this->add_control(
				'selected_icon',
				[
					'label' => esc_html__( 'Icon', 'elementor' ),
					'type' => Controls_Manager::ICONS,
					'fa4compatibility' => 'icon',
					'default' => [
						'value' => 'fas fa-play',
						'library' => 'fa-solid',
					],
				]
			);
			$this->add_control(
				'selected_icon_view',
				[
					'label' => esc_html__( 'Display ', 'elementor' ),
					'type' => Controls_Manager::SELECT,
					'options' => [
						'default' => esc_html__( 'Default', 'elementor' ),
						'stacked' => esc_html__( 'Stacked', 'elementor' ),
						'framed' => esc_html__( 'Framed', 'elementor' ),
					],
					'default' => 'framed',
					'prefix_class' => 'selected-view-',
				]
			);


			$this->add_control(
				'selected_icon_shape',
				[
					'label' => esc_html__( 'Shape', 'elementor' ),
					'type' => Controls_Manager::SELECT,
					'options' => [
						'circle' => esc_html__( 'Circle', 'elementor' ),
						'square' => esc_html__( 'Square', 'elementor' ),
					],
					'default' => 'circle',
					'condition' => [
						'selected_icon_view!' => 'default',
					],
					'prefix_class' => 'selected-shape-',
				]
			);

			$this->add_control(
				'selected_icon_icon_color',
				[
					'label' => __( 'Primary Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .learndash-lasson-continue-widget-icon-style.selected i' => 'color: {{VALUE}}',
						'{{WRAPPER}} .learndash-lasson-continue-widget-icon-style.selected svg' => 'fill: {{VALUE}}',
					],
					'default' => '#fff',
				]
			);
			$this->add_control(
				'selected_icon_back_color',
				[
					'label' => __( 'Secondary Color', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .learndash-lasson-continue-widget-icon-style.selected i, {{WRAPPER}} .learndash-lasson-continue-widget-icon-style.selected svg' => 'background-color: {{VALUE}}',
					],
					'condition' => [
						'selected_icon_view!' => 'default',
					],
				]
			);

			$this->add_group_control(
				\Elementor\Group_Control_Border::get_type(),
				[
					'name' => 'selected_border_width',
					'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
					'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-icon-style.selected i, {{WRAPPER}} .learndash-lasson-continue-widget-icon-style.selected svg',
					'condition' => [
						'selected_icon_view' => 'framed',
					],
				]
			);
				$this->add_responsive_control(
				'selected_icon_padding',
				[
					'label' => __( 'Padding', 'plugin-domain' ),
					'type' => Controls_Manager::DIMENSIONS,

					'size_units' => [ 'px'  ],
					'default' => [
						'top' => 17,
						'right' => 17,
						'bottom' => 17,
						'left' => 20,
					],
					'condition' => [
						'selected_icon_view!' => 'default',
					],
					'selectors' => [
						'{{WRAPPER}} .learndash-lasson-continue-widget-icon-style.selected i, {{WRAPPER}} .learndash-lasson-continue-widget-icon-style.selected svg' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);
        $this->end_controls_section();
        $this->start_controls_section(
            'content_style_section',
            [
                'label' => __( 'Content', 'elementor-custom-widgets-for-learndash' ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
		$this->add_control(
			'heading_3',
			[
				'label' => __( 'Course Title', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
			]
		);
        // Title Color
		$this->add_control(
			'main_title_color',
			[
				'label' => __( 'Text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-main-title-style' => 'color: {{VALUE}}',
				],
			]
		);
		//Title Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'main_title_text_typography',
				'label' => __( 'Text Typography', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-main-title-style ',
			]
		);
		$this->add_control(
			'main_title_text_padding',
			[
				'label' => __( 'Padding', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
						'step' => 1,
					],
				],
				'default' => [
					'size' => 10,
				],
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-main-title-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],

			]
		);


		$this->add_control(
			'heading_4',
			[
				'label' => __( 'Post Title', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
                'separator' => 'before',
			]
		);
        // Title Color
		$this->add_control(
			'title_color',
			[
				'label' => __( 'Text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-title-style' => 'color: {{VALUE}}',
				],
			]
		);
		//Title Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_text_typography',
				'label' => __( 'Text Typography', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-title-style ',
			]
		);
		$this->add_control(
			'title_text_padding',
			[
				'label' => __( 'Padding', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
						'step' => 1,
					],
				],
				'default' => [
					'size' => 10,
				],
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-title-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],

			]
		);

		$this->add_control(
			'heading_5',
			[
				'label' => __( 'Excerpt', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
                'separator' => 'before',
			]
		);
        // excerpt Color
		$this->add_control(
			'excerpt_color',
			[
				'label' => __( 'Text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-excerpt-style' => 'color: {{VALUE}}',
				],
			]
		);
		//excerpt Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'excerpt_text_typography',
				'label' => __( 'Text Typography', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-excerpt-style',
			]
		);
		$this->add_control(
			'excerpt_lenght',
			[
				'label' => __( 'Excerpt Length', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 0,
				'max' => 700,
				'step' => 1,
				'default' => 150,

			]
		);
		$this->add_control(
			'excerpt_text_padding',
			[
				'label' => __( 'Padding', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'default' => [
					'size' => 30,
				],
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-excerpt-style' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],

			]
		);

		$this->add_control(
			'heading_6',
			[
				'label' => __( 'link', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::HEADING,
                'separator' => 'before',
			]
		);
        // link Color
		$this->add_control(
			'link_color',
			[
				'label' => __( 'Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-link-style' => 'color: {{VALUE}}',
				],
				'default' => '#fff',
			]
		);
		//link background
		$this->add_control(
			'link_back_color',
			[
				'label' => __( 'Background Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-link-style' => 'background-color: {{VALUE}}',
				],
				'default' => '#93013c',
			]
		);

		//link Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'link_text_typography',
				'label' => __( 'Text Typography', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-link-style ',
			]
		);
		// link Padding
		$this->add_responsive_control(
			'link_padding',
			[
				'label' => __( 'Padding', 'plugin-domain' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'default' => [
					'top' => 12,
					'right' => 27,
					'bottom' => 12,
					'left' => 27,
				],
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-link-style' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		//link border
		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'link_border',
				'label' => __( 'Border', 'elementor-custom-widgets-for-learndash' ),
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-link-style',
			]
		);
		 //link border color
		 $this->add_control(
			'link_border_color',
			[
				'label' => __( 'Border Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-link-style' => 'border-color: {{VALUE}}',
				],
				'default' => '#93013c',
			]
		);
        //content position
        // $this->add_control(
		// 	'content_block_position',
		// 	[
		// 		'label' => esc_html__( 'Content Position', 'elementor-custom-widgets-for-learndash' ),
		// 		'type' => \Elementor\Controls_Manager::SELECT,
        //         'dynamic' => [
		// 			'active' => true,
		// 		],
		// 		'options' => [
        //             'content_block_position_default' => esc_html__( 'Bottom Left', 'elementor-custom-widgets-for-learndash' ),
        //         'content_block_position_top_left' => esc_html__( 'Top Left', 'elementor-custom-widgets-for-learndash' ),
		// 		'content_block_position_bottom_right' => esc_html__( 'Bottom Right', 'elementor-custom-widgets-for-learndash' ),
        //         'content_block_position_top_right' => esc_html__( 'Top Right', 'elementor-custom-widgets-for-learndash' ),
		// 		],
		// 		'default' => 'content_block_position_default',
		// 	]
		// );
        $this->end_controls_section();
        $this->start_controls_section(
			'progress_tap',
			[
				'label' => esc_html__( 'Progress', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
        $this->add_control(
			'progress_show_value',
			[
				'label' => __( 'Show Progress Value', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Showe', 'elementor-custom-widgets-for-learndash' ),
				'label_off' => __( 'Hide', 'elementor-custom-widgets-for-learndash' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$this->add_control(
			'progress_background_stroke_color',
			[
				'label' => __( 'Background Stroke Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					  '{{WRAPPER}} .learndash-lasson-continue-widget .list-option .progress-back.ProgressBar-background' => 'background: {{VALUE}}',
					'{{WRAPPER}} .learndash-lasson-continue-widget-progress-style .ProgressBar-background' => 'stroke: {{VALUE}}',
				],
				'default' => '#fff',
			]
		);
		$this->add_control(
			'progress_value_stroke_color',
			[
				'label' => __( 'Value Stroke Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget .list-option .progress-strocke.ProgressBar-circle' => 'background: {{VALUE}}',
					'{{WRAPPER}} .learndash-lasson-continue-widget-progress-style .ProgressBar-circle' => 'stroke: {{VALUE}}',
				],
				'default' => '#93013c',
			]
		);
		$this->add_control(
			'progress_background_color',
			[
				'label' => __( 'Background Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .learndash-lasson-continue-widget-progress-style .ProgressBar-circle' => 'fill: {{VALUE}}',
				],
				'default' => 'transparent',
			]
		);
		$this->add_control(
			'progress_text_color',
			[
				'label' => __( 'Text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .list-option .ProgressBar-percentage' => 'color: {{VALUE}}',
					'{{WRAPPER}} .learndash-lasson-continue-widget-progress-style .ProgressBar-percentage' => 'color: {{VALUE}}',
				],
				'default' => '#fff',
			]
		);
		//Progress Text Typography
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'progress_text_typography',
				'label' => __( 'Value Typography', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .learndash-lasson-continue-widget-progress-style .ProgressBar-percentage, {{WRAPPER}} .list-option .ProgressBar-percentage',
			]
		);
			//content position
			$this->add_control(
			'progress_position',
			[
				'label' => esc_html__( 'Content Position', 'elementor-custom-widgets-for-learndash' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'dynamic' => [
					'active' => true,
				],
				'options' => [
					'default' => esc_html__( 'Top Left', 'elementor-custom-widgets-for-learndash' ),
				'bottom-left' => esc_html__( 'Bottom Left', 'elementor-custom-widgets-for-learndash' ),
				'bottom-right' => esc_html__( 'Bottom Right', 'elementor-custom-widgets-for-learndash' ),
				'top-right' => esc_html__( 'Top Right', 'elementor-custom-widgets-for-learndash' ),
				],
				'default' => 'default',
			]
		);
		$this->add_responsive_control(
                'progress_padding',
                [
                    'label' => __( 'Padding', 'plugin-domain' ),
                    'type' => Controls_Manager::DIMENSIONS,
                    'size_units' => [ 'px', '%', 'em' ],
                    'default' => [
                        'top' => 0,
                        'right' => 0,
                        'bottom' => 0,
                        'left' => 0,
                    ],
                    'selectors' => [
                        '{{WRAPPER}} .learndash-lasson-continue-widget-progress-style' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                    ],
                ]
            );
            $this->add_control(
				'circle_size',
				[
					'label' => __( 'Circle Size', 'plugin-domain' ),
					'type' => Controls_Manager::SLIDER,
					'separator'=>'before',
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 80,
							'step' => 1,
						],
					],
					'default' => [
						'size' =>49
					],
					'selectors' => [
						'{{WRAPPER}} .learndash-lasson-continue-widget .learndash-lasson-continue-widget-progress-style  .ProgressBar, {{WRAPPER}} .learndash-lasson-continue-widget .learndash-lasson-continue-widget-progress-style .ProgressBar-contentCircle' => 'width: {{SIZE}}px; height: {{SIZE}}px;',
					],

				]
			);

        $this->end_controls_section();

    }
    private function style_tab(){

    }
    protected function render(){
		$this->get_latest_user_activity();
		$settings = $this->get_settings_for_display();
		$course_id= $settings['course_id'];
		$course_modules=$settings['hidden_course_all_modules'];
		$user_last_lessons= $settings['hidden_user_activity'];
        ?>
          <div class="learndash-lasson-continue-widget">
		  <?php
             foreach($course_modules as $course_module){
                $short_excerpt= substr( $course_module['module_excerpt'],0, $settings['excerpt_lenght']);
               if($course_module['course_id']== $course_id){
				if($course_module['module_id'] == $user_last_lessons[$course_id]){
                     include  ELCW_PLUGIN_PATH_template.'/learndash-lesson-continue-template.php';
                 }
				}
              }
            ?>
        </div>
<?php
    }
    protected function _content_template() {
		?><#
		var selectedIcon = elementor.helpers.renderIcon( view, settings.selected_icon, { 'aria-hidden': true }, 'i' , 'object' );
		var course_modules=settings.hidden_course_all_modules ;
		var course_id=  settings.course_id;
		var user_last_lessons= settings.hidden_user_activity;   #>

		<div class="learndash-lasson-continue-widget">
		<# _.each(course_modules , function(course_module){
                if(course_module.course_id ==  course_id){
					if( course_module.module_id == user_last_lessons[course_id]){
						let text= course_module.module_excerpt;
						if(text){
					 var short_excerpt= text.substr( 0,  settings.excerpt_lenght );
                    }
                    else{
                        var short_excerpt= "";
                    }
						#>
                <?php  include  ELCW_PLUGIN_PATH_template.'/learndash-lesson-continue-template-frontend.php'; ?>
                <#
				 }
              }
			})
         #>
		</div>

		<?php
    }
}
Plugin::instance()->widgets_manager->register_widget_type( new LEARNDASH_LESSON_CONTINUE_WIDGET());