; (function ($) {
	var courseNavigationHandler = function ($scope, $) {
		/* This is your custom Javascript */
		$('document').ready(function () {
			$('.learndash-course-navigation-widget .accordion').each(function () {
				var allContent = $(this).find('.ac-content');
				var titleElement = $(this).find('.ac-title');
				titleElement.each(function () {
					var titleID = $(this).attr('id');
					$('#' + titleID ).children('span.widget-course-navigation-icon-collapse-style.collapse').unbind('click').bind('click', function () {
						var contentID = $(this).parent( ".ac-title" ).next().attr('id');
						$('#' + titleID).removeClass('open-collapse');
						if ($('#' + contentID).hasClass('hide-content')) {
							titleElement.removeClass('open-collapse');
							allContent.removeClass('show-content');
							allContent.addClass('hide-content');
							$('#' + contentID).removeClass('hide-content');
							$('#' + contentID).addClass('show-content');
							$('#' + titleID).addClass(' open-collapse');
						}
						else {
							titleElement.removeClass('open-collapse');
							allContent.addClass('hide-content');
							$('#' + contentID).addClass('hide-content');
							$('#' + contentID).removeClass('show-content');
						}
					});

				});
			});

			$('.learndash-course-navigation-widget .accordion .ac-container').each(function () {
				if ($(this).find('.ac-content ul li').length == 0) {
					$(this).find('.ac-content ul').remove();
				}
			});

		});
	};
	var OwelCarouselHandler = function ($scope, $) {
		$(document).ready(function () {
			$(".owl-carousel").each(function () {
				var responsive = $(this).data('responsive');
				$(this).owlCarousel({
					loop: true,
					responsive: responsive,
					nav: true,
					navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"]
				});
			});
		});
	};
	var circleProgressHandler = function ($scope, $) {
		$(document).ready(function () {
			$('.ProgressBar.ProgressBar--animateNone').each(function () {
				var $this = $(this);
				var $progressBar = $this;
				var $progressCount = $progressBar.find('.ProgressBar-percentage--count');
				var $circle = $progressBar.find('.ProgressBar-circle');
				var percentageProgress = $progressBar.attr('data-progress');
				const percentageAnimation = $progressBar.attr('data-animation-duration');
				var percentageRemaining = (100 - percentageProgress);
				var percentageText = $progressCount.parent().attr('data-progress');

				//Calcule la circonférence du cercle
				var radius = $circle.attr('r');
				var diameter = radius * 2;
				var circumference = Math.round(Math.PI * diameter);

				//Calcule le pourcentage d'avancement
				var percentage = circumference * percentageRemaining / 100;

				$circle.css({
					'stroke-dasharray': circumference,
					'stroke-dashoffset': percentage
				})


				$progressCount.text(percentageText + '%');
			});
		});
	};
	var clickableDiv = function ($scope, $) {
	 $('document').ready(function () {
			$('body:not(.elementor-editor-active) .clickable-div').click(function(){
				var customLink= $(this).attr('data-link');
				window.location = customLink;
			});

	 });
	};


	$(window).on('elementor/frontend/init', function () {
		elementorFrontend.hooks.addAction('frontend/element_ready/learndash-course-grid-widget.default', OwelCarouselHandler);
		elementorFrontend.hooks.addAction('frontend/element_ready/learndash-course-navigation-widget.default', courseNavigationHandler);
		elementorFrontend.hooks.addAction('frontend/element_ready/learndash-course-grid-widget.default', circleProgressHandler);
		elementorFrontend.hooks.addAction('frontend/element_ready/learndash-circle-progress-widget.default', circleProgressHandler);
		elementorFrontend.hooks.addAction('frontend/element_ready/learndash-lesson-continue-widget.default', circleProgressHandler);
		elementorFrontend.hooks.addAction('frontend/element_ready/learndash-course-navigation-widget.default', circleProgressHandler);
		elementorFrontend.hooks.addAction('frontend/element_ready/learndash-lesson-continue-widget.default', clickableDiv);
		elementorFrontend.hooks.addAction('frontend/element_ready/learndash-course-grid-widget.default', clickableDiv);
	});

})(jQuery);